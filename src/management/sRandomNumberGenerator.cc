/*
This file is part of CASToR.

    CASToR is free software: you can redistribute it and/or modify it under the
    terms of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option) any later
    version.

    CASToR is distributed in the hope that it will be useful, but WITHOUT ANY
    WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
    details.

    You should have received a copy of the GNU General Public License along with
    CASToR (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.

Copyright 2017 all CASToR contributors listed below:

    --> current contributors: Thibaut MERLIN, Simon STUTE, Didier BENOIT, Marina FILIPOVIC
    --> past contributors: Valentin VIELZEUF

This is CASToR version 1.2.
*/

/*
  Implementation of class sRandomNumberGenerator

  - separators: X
  - doxygen: X
  - default initialization: X
  - CASTOR_DEBUG: none
  - CASTOR_VERBOSE: X
*/

/*!
  \file
  \ingroup management

  \brief Implementation of class sRandomNumberGenerator
*/


#include "sRandomNumberGenerator.hh"

sRandomNumberGenerator *sRandomNumberGenerator::mp_Instance = NULL;


// =====================================================================
// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
// =====================================================================
/*!
  \brief Constructor of sRandomNumberGenerator. Do nothing by default as it is a singleton clasee
*/
sRandomNumberGenerator::sRandomNumberGenerator()
{
  mp_Instance = NULL;
  m_verbose = 5;
  mp_Engines.clear();
  mp_NonThreadedEngine = NULL;
}



// =====================================================================
// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
// =====================================================================
/*
  \brief Destructor of sRandomNumberGenerator. Do nothing by default
*/
sRandomNumberGenerator::~sRandomNumberGenerator()
{
  if(mp_NonThreadedEngine) delete mp_NonThreadedEngine;
}



// =====================================================================
// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
// =====================================================================
/*
  \fn Initialize
  \param a_nbThreads
  \brief Instanciate a number of RNG according to the number of threads used in openMP
  \details It uses a std::random_device as initial seed for each thread
  \return 0 if success, positive value otherwise
*/
int sRandomNumberGenerator::Initialize(int a_nbThreads)
{
  if(m_verbose >=3) if(m_verbose>=3) Cout("sRandomNumberGenerator::Initialize ..."<< endl); 
  
  for(int th=0; th<a_nbThreads; th++)
  {
    std::random_device rd;
    mp_Engines.push_back(Engine(rd()));
  }

  std::random_device rd_single;
  mp_NonThreadedEngine = new Engine(rd_single());
    
  return 0;
}



// =====================================================================
// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
// =====================================================================
/*!
  \fn Initialize
  \param a_seed
  \param a_nbThreads
  \brief Instantiate a number of RNG according to the number of threads used in openMP, and a provided seed
  \details It uses the seed provided in argument for initialization. 
  \details If multitheading is used, the seed is just incremented for the initialization of the engine of each thread.
  \return 0 if success, positive value otherwise
*/
int sRandomNumberGenerator::Initialize(int64_t a_seed, int a_nbThreads)
{
  if(m_verbose >=3) Cout("sRandomNumberGenerator::Initialize with provided seed..."<< endl); 
    
  if (a_seed<0)
  {
    Cout("***** sRandomNumberGenerator::Initialize()-> Error : seed for RNG should be >=0 !" << endl);
    return 1;
  }
  
  for(int th=0; th<a_nbThreads; th++)
  {
    mp_Engines.push_back(Engine(a_seed));
    if(m_verbose>=3) Cout("sRandomNumberGenerator::Initialize()->Seed for thread "<< th << " : " << a_seed << endl);
    a_seed++;
  }

  mp_NonThreadedEngine = new Engine(a_seed);

  return 0;
}



// =====================================================================
// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
// =====================================================================
/*
  \fn GenerateRdmNber
  \brief Generate a random number for the thread whose index is recovered from the ompenMP function
  \todo Perhaps getting the thread index from argument rather than directly from the function.
  \todo But this implementation allows the RNG to be used anywhere in the code
  \todo Perhaps create a random distribution on the fly, and offer the possibility to select lower/upper bounds via argument parameters
  \return a random generated number in [0. ; 1.)
*/
double sRandomNumberGenerator::GenerateRdmNber()
{
  #ifdef CASTOR_VERBOSE
  if(m_verbose >=4) Cout("sRandomNumberGenerator::GenerateRdmNber..."<< endl); 
  #endif
  
  int id=0;
  #ifdef CASTOR_OMP
  id = omp_get_thread_num();
  #endif
  return mp_Distribution(mp_Engines[id]);
} 



// =====================================================================
// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
// =====================================================================
/*
  \fn GenerateNonThreadedRdmNber
  \brief Generate a random number using the not thread safe random generator, for use in sequential
  parts of an otherwise multithreaded code
  \return double random number
*/
double sRandomNumberGenerator::GenerateNonThreadedRdmNber()
{
  #ifdef CASTOR_VERBOSE
  if(m_verbose >=4) Cout("sRandomNumberGenerator::GenerateNonThreadedRdmNber..."<< endl); 
  #endif
  
  return mp_Distribution((*mp_NonThreadedEngine));
} 



// =====================================================================
// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
// =====================================================================
/*
  \fn GetNonThreadedGenerator
  \brief Get the not thread safe random generator, for use in sequential
  parts of an otherwise multithreaded code
  \return double random number
*/
sRandomNumberGenerator::Engine* sRandomNumberGenerator::GetNonThreadedGenerator()
{
   #ifdef CASTOR_VERBOSE
  if(m_verbose >=4) Cout("sRandomNumberGenerator::GetNonThreadedGenerator..."<< endl); 
  #endif
  
  return mp_NonThreadedEngine;
} 
