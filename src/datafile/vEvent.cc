/*
This file is part of CASToR.

    CASToR is free software: you can redistribute it and/or modify it under the
    terms of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option) any later
    version.

    CASToR is distributed in the hope that it will be useful, but WITHOUT ANY
    WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
    details.

    You should have received a copy of the GNU General Public License along with
    CASToR (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.

Copyright 2017 all CASToR contributors listed below:

    --> current contributors: Thibaut MERLIN, Simon STUTE, Didier BENOIT, Marina FILIPOVIC
    --> past contributors: Valentin VIELZEUF

This is CASToR version 1.2.
*/

/*
  Implementation of class vEvent

  - separators: X
  - doxygen: X
  - default initialization: X
  - CASTOR_DEBUG: none
  - CASTOR_VERBOSE: none
*/

/*!
  \file
  \ingroup datafile

  \brief Implementation of class vEvent
*/


#include "vEvent.hh"
#include "vDataFile.hh"
#include "sOutputManager.hh"


// =====================================================================
// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
// =====================================================================
/*
  Constructor()
*/
vEvent::vEvent()
{
  m_timeInMs=0;
  m_nbLines=0;
  mp_ID1=NULL;
  mp_ID2=NULL;
  m_dataType = TYPE_UNKNOWN;
  m_dataMode = MODE_UNKNOWN;
  m_verbose=-1;
  m_eventValue = 0.;
}



// =====================================================================
// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
// =====================================================================
/*
  Destructor()
*/
vEvent::~vEvent() 
{
  if(mp_ID1 != NULL) delete mp_ID1;
  if(mp_ID1 != NULL) delete mp_ID2;
}



// =====================================================================
// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
// =====================================================================
/*
  \fn AllocateID()
  \brief Instantiate the mp_ID1 and mp_ID2 indices arrays
  \details This function instantiate the mp_ID1 and mp_ID2 indices arrays using the m_nbLines filed (assuming it has been initialized before,
           and call the AllocateSpecificData() function implemented in child classes
*/
int vEvent::AllocateID() 
{
  if(m_verbose >=3)
    Cout("vEvent::AllocateID()..." << endl);
    
  if (m_nbLines<1)
  {
    Cerr("*****vEvent::AllocateID() -> Error, number of lines has not been initialized (<1) !" << endl);
    return 1;
  }
  else
  {
    mp_ID1 = new uint32_t[m_nbLines];
    mp_ID2 = new uint32_t[m_nbLines];
  }
  
  // Call the pure virtual function implemented in child classes for the allocation of data specific to child classes
  if (AllocateSpecificData())
  {
    Cerr("*****vEvent::AllocateID() -> Error when trying to allocated specific data for the Event !" << endl);
    return 1;
  }
  
  return 0;
}
