/*
This file is part of CASToR.

    CASToR is free software: you can redistribute it and/or modify it under the
    terms of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option) any later
    version.

    CASToR is distributed in the hope that it will be useful, but WITHOUT ANY
    WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
    details.

    You should have received a copy of the GNU General Public License along with
    CASToR (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.

Copyright 2017 all CASToR contributors listed below:

    --> current contributors: Thibaut MERLIN, Simon STUTE, Didier BENOIT, Marina FILIPOVIC
    --> past contributors: Valentin VIELZEUF

This is CASToR version 1.2.
*/

/*
  Implementation of class iEventHistoPET

  - separators: X
  - doxygen: X
  - default initialization: X
  - CASTOR_DEBUG: none
  - CASTOR_VERBOSE: none
*/


/*!
  \file
  \ingroup datafile

  \brief Implementation of class iEventHistoPET
*/


#include "iEventHistoPET.hh"
#include "vDataFile.hh" //  DATA_MODE/ DATA_TYPE macros
#include "sOutputManager.hh" // Cout()/Cerr()


// =====================================================================
// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
// =====================================================================
/*
  Constructor()
*/
iEventHistoPET::iEventHistoPET() : iEventPET()
{
  m_dataType = TYPE_PET;
  m_dataMode = MODE_HISTOGRAM;
  m_eventNbTOFBins = 1;
  mp_eventValue =NULL; 
  mp_eventScatRate =NULL;
  m_nbLines = 1;
}


// =====================================================================
// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
// =====================================================================
/*
  Destructor()
*/
iEventHistoPET::~iEventHistoPET() 
{
  if(mp_eventValue != NULL) delete mp_eventValue;
  if(mp_eventScatRate != NULL) delete mp_eventScatRate;
}



// =====================================================================
// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
// =====================================================================
/*
  \fn virtual int AllocateSpecificData()
  \brief Function allowing the allocation of specific data.
         Instantiate and initialize the mp_eventValue and mp_eventScatIntensity arrays depending of the number of TOF bins
  \return 0 is success, positive value otherwise
*/
int iEventHistoPET::AllocateSpecificData()
{
  if(m_eventNbTOFBins<1)
  {
    Cerr("*****iEventHistoPET::AllocateSpecificData() -> Error, number of TOF bins has not been initialized (<1) !");
    return 1;
  }

  mp_eventValue = new FLTNB[m_eventNbTOFBins];
  mp_eventScatRate = new FLTNB[m_eventNbTOFBins]; 
  
  for(int tb=0 ; tb<m_eventNbTOFBins ; tb++)
  {
    mp_eventValue[tb] = 1.;
    mp_eventScatRate[tb] = 0.;
  }
  
  return 0;
}



// =====================================================================
// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
// =====================================================================
/*
  \fn virtual FLTNB GetAdditiveCorrections()
  \return the sum of additive correction terms, summed for the given TOF bins
*/
FLTNB iEventHistoPET::GetAdditiveCorrections(int a_bin)
{    
  return mp_eventScatRate[a_bin] + m_eventRdmRate/((FLTNB)m_eventNbTOFBins);
}



// =====================================================================
// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
// =====================================================================
/*
  \fn virtual void Describe()
  \brief This function can be used to get a description of the event printed out
*/
void iEventHistoPET::Describe()
{
  Cout("---------------------- iEventHistoPET::Describe() --------------------------" << endl);
  Cout("sizeof(FLTNB): " << sizeof(FLTNB) << endl);
  // From vEvent
  //  uint32_t m_timeInMs; /*!< Timestamp of the event in ms. Default value =0 */
  Cout("Time: " << m_timeInMs << " ms" << endl);
  //  uint16_t m_nbLines; /*!< Number of lines in the event. Default value =0 */
  Cout("Number of lines: " << m_nbLines << endl);
  //  uint32_t* mp_ID1; /*!< Pointer containing the indice(s) of the 1st ID of the Event. Default value =NULL  */
  //  uint32_t* mp_ID2; /*!< Pointer containing the indice(s) of the 2nd ID of the Event. Default value =0 */
  for (uint16_t l=0; l<m_nbLines; l++) Cout("  --> ID1: " << mp_ID1[l] << " | ID2: " << mp_ID2[l] << endl);
  // From iEventPET
  //  FLTNB m_eventRdmRate; /*!< Correction term for randoms rate (unit: s-1). Default value =0.0 */
  Cout("Random rate: " << m_eventRdmRate << endl);
  //  FLTNB m_eventNormFactor; /*!< Normalization term. Default value =1.0 */
  Cout("Normalization factor: " << m_eventNormFactor << endl);
  //  FLTNB m_atnCorrFactor; /*!< Correction term for attenuation. Default value =1.0 */
  Cout("ACF: " << m_atnCorrFactor << endl);
  // From iEventHistoPET
  //  uint16_t m_eventNbTOFBins; /*!< Number of TOF bins in the Event. Default value =1 */
  Cout("Number of TOF bins: " << m_eventNbTOFBins << endl);
  //  FLTNB* mp_eventValue; /*!< Pointer containing the amount of data in each potential TOF bin. Default value =1.0 */
  //  FLTNB* mp_eventScatRate; /*!< Pointer containing the scatter correction term (as a rate in s-1) for each potential TOF bin. Default value =0.0 */
  for (uint16_t t=0; t<m_eventNbTOFBins; t++) Cout("  --> Event value: " << mp_eventValue[t] << " | Scatter rate: " << mp_eventScatRate[t] << endl);
  Cout("----------------------------------------------------------------------------" << endl);
  Cout(flush);
}
