/*
This file is part of CASToR.

    CASToR is free software: you can redistribute it and/or modify it under the
    terms of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option) any later
    version.

    CASToR is distributed in the hope that it will be useful, but WITHOUT ANY
    WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
    details.

    You should have received a copy of the GNU General Public License along with
    CASToR (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.

Copyright 2017 all CASToR contributors listed below:

    --> current contributors: Thibaut MERLIN, Simon STUTE, Didier BENOIT, Marina FILIPOVIC
    --> past contributors: Valentin VIELZEUF

This is CASToR version 1.2.
*/

/*
  Implementation of class iEventTransmission

  - separators: 
  - doxygen: 
  - default initialization: 
  - CASTOR_DEBUG: 
  - CASTOR_VERBOSE: 
*/

/*!
  \file
  \ingroup datafile

  \brief Implementation of class iEventTransmission
*/

#include "iEventTransmission.hh"
#include "sOutputManager.hh"

// =====================================================================
// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
// =====================================================================

iEventTransmission::iEventTransmission() : vEvent()
{
  // Throw an error
  Cerr("***** iEventTransmission::iEventTransmission() -> Not yet implemented !" << endl);
  Exit(EXIT_FAILURE);
}

// =====================================================================
// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
// =====================================================================

iEventTransmission::~iEventTransmission() {}
