/*
This file is part of CASToR.

    CASToR is free software: you can redistribute it and/or modify it under the
    terms of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option) any later
    version.

    CASToR is distributed in the hope that it will be useful, but WITHOUT ANY
    WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
    details.

    You should have received a copy of the GNU General Public License along with
    CASToR (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.

Copyright 2017 all CASToR contributors listed below:

    --> current contributors: Thibaut MERLIN, Simon STUTE, Didier BENOIT, Marina FILIPOVIC
    --> past contributors: Valentin VIELZEUF

This is CASToR version 1.2.
*/

/*
  Implementation of class iProjectorIncrementalSiddon

  - separators: done
  - doxygen: done
  - default initialization: done
  - CASTOR_DEBUG: 
  - CASTOR_VERBOSE: 
*/

/*!
  \file
  \ingroup  projector
  \brief    Implementation of class iProjectorIncrementalSiddon
*/

#include "iProjectorIncrementalSiddon.hh"
#include "sOutputManager.hh"

// =====================================================================
// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
// =====================================================================

iProjectorIncrementalSiddon::iProjectorIncrementalSiddon() : vProjector()
{
  // This projector is compatible with SPECT attenuation correction because
  // all voxels contributing to a given line are ordered from point 1 (focal)
  // to point 2 (scanner). Note that there can be some aliasing problem with
  // this incremental method, but which will depend on the voxel size and number.
  // However, these problems are quite negligible and pretty uncommon. So we still
  // say that this projector is compatible.
  m_compatibleWithSPECTAttenuationCorrection = true;
}

// =====================================================================
// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
// =====================================================================

iProjectorIncrementalSiddon::~iProjectorIncrementalSiddon()
{
}

// =====================================================================
// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
// =====================================================================

int iProjectorIncrementalSiddon::ReadConfigurationFile(const string& a_configurationFile)
{
  // No options for incremental siddon
  ;
  // Normal end
  return 0;
}

// =====================================================================
// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
// =====================================================================

int iProjectorIncrementalSiddon::ReadOptionsList(const string& a_optionsList)
{
  // No options for incremental siddon
  ;
  // Normal end
  return 0;
}

// =====================================================================
// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
// =====================================================================

void iProjectorIncrementalSiddon::ShowHelpSpecific()
{
  cout << "This projector is a simple line projector that computes the exact path length of a line through the voxels." << endl;
  cout << "It is basically an incremental version of the original algorithm proposed by R. L. Siddon (see the classicSiddon projector)." << endl;
  cout << "It is implemented from the following published paper:" << endl;
  cout << "F. Jacobs et al, \"A fast algorithm to calculate the exact radiological path through a pixel or voxel space\", J. Comput. Inf. Technol., vol. 6, pp. 89-94, 1998." << endl;
}

// =====================================================================
// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
// =====================================================================

int iProjectorIncrementalSiddon::CheckSpecificParameters()
{
  // Nothing to check for this projector
  ;
  // Normal end
  return 0;
}

// =====================================================================
// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
// =====================================================================

int iProjectorIncrementalSiddon::InitializeSpecific()
{
  // Verbose
  if (m_verbose>=2) Cout("iProjectorIncrementalSiddon::InitializeSpecific() -> Use incremental Siddon projector" << endl);
  // Normal end
  return 0;
}

// =====================================================================
// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
// =====================================================================

INTNB iProjectorIncrementalSiddon::EstimateMaxNumberOfVoxelsPerLine()
{
  // Find the maximum number of voxels along a given dimension
  INTNB max_nb_voxels_in_dimension = mp_ImageDimensionsAndQuantification->GetNbVoxX();
  if (mp_ImageDimensionsAndQuantification->GetNbVoxY()>max_nb_voxels_in_dimension) max_nb_voxels_in_dimension = mp_ImageDimensionsAndQuantification->GetNbVoxY();
  if (mp_ImageDimensionsAndQuantification->GetNbVoxZ()>max_nb_voxels_in_dimension) max_nb_voxels_in_dimension = mp_ImageDimensionsAndQuantification->GetNbVoxZ();
  // We should have at most 4 voxels in a given plane, so multiply by 4
  // (note: this is not true however it ensures no overflow and is already quite optimized for RAM usage !)
  max_nb_voxels_in_dimension *= 4;
  // Return the value
  return max_nb_voxels_in_dimension;
}

// =====================================================================
// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
// =====================================================================

int iProjectorIncrementalSiddon::ProjectWithoutTOF(int a_direction, oProjectionLine* ap_ProjectionLine )
{
  #ifdef CASTOR_DEBUG
  if (!m_initialized)
  {
    Cerr("***** iProjectorIncrementalSiddon::ProjectWithoutTOF() -> Called while not initialized !" << endl);
    Exit(EXIT_DEBUG);
  }
  #endif

  #ifdef CASTOR_VERBOSE
  if (m_verbose>=10)
  {
    string direction = "";
    if (a_direction==FORWARD) direction = "forward";
    else direction = "backward";
    Cout("iProjectorIncrementalSiddon::Project without TOF -> Project line '" << ap_ProjectionLine << "' in " << direction << " direction" << endl);
  }
  #endif

  // Get event positions
  FLTNB* event1Float = ap_ProjectionLine->GetPosition1();
  FLTNB* event2Float = ap_ProjectionLine->GetPosition2();
  double event1[3] = { event1Float[0], event1Float[1], event1Float[2] };
  double event2[3] = { event2Float[0], event2Float[1], event2Float[2] };

  // **************************************
  // STEP 1: LOR length calculation
  // **************************************
  double length_LOR = ap_ProjectionLine->GetLength();

  // **************************************
  // STEP 2: Compute entrance voxel indexes
  // **************************************
  double alphaFirst[] = { 0.0, 0.0, 0.0 };
  double alphaLast[] = { 0.0, 0.0, 0.0 };

  double alphaMin = 0.0f, alphaMax = 1.0f;
  double delta_pos[] = {
    event2[ 0 ] - event1[ 0 ],
    event2[ 1 ] - event1[ 1 ],
    event2[ 2 ] - event1[ 2 ]
  };

  // Computation of alphaMin et alphaMax values (entrance and exit point of the ray)
  for( int i = 0; i < 3; ++i )
  {
    if( delta_pos[ i ] != 0.0 )
    {
      alphaFirst[i] = (-mp_halfFOV[i] - event1[i]) / (delta_pos[ i ]);
      alphaLast[i]  = ( mp_halfFOV[i] - event1[i]) / (delta_pos[ i ]);
      alphaMin = (std::max)(alphaMin,(std::min)(alphaFirst[i],alphaLast[i]));
      alphaMax = (std::min)(alphaMax,(std::max)(alphaFirst[i],alphaLast[i]));
    }
  }

  // if alphaMax is less than or equal to alphaMin no intersection
  // and return an empty buffer
  if( alphaMax <= alphaMin ) return 0;

  // Now we have to find the indices of the particular plane
  // (iMin,iMax), (jMin,jMax), (kMin,kMax)
  int iMin = 0, iMax = 0;
  int jMin = 0, jMax = 0;
  int kMin = 0, kMax = 0;

  // For the X-axis
  if( delta_pos[ 0 ] > 0.0f )
  {
    iMin = ::ceil( ( mp_nbVox[ 0 ] + 1 ) - ( mp_halfFOV[ 0 ] - alphaMin * delta_pos[ 0 ] - event1[ 0 ] ) / mp_sizeVox[0] );
    iMax = ::floor( 1 + ( event1[ 0 ] + alphaMax * delta_pos[ 0 ] - (-mp_halfFOV[ 0 ]) ) / mp_sizeVox[0] );
  }
  else if( delta_pos[ 0 ] < 0.0f )
  {
    iMin = ::ceil( ( mp_nbVox[ 0 ] + 1 ) - ( mp_halfFOV[ 0 ] - alphaMax * delta_pos[ 0 ] - event1[ 0 ] ) / mp_sizeVox[0] );
    iMax = ::floor( 1 + ( event1[ 0 ] + alphaMin * delta_pos[ 0 ] - (-mp_halfFOV[ 0 ]) ) / mp_sizeVox[0] );
  }
  if( delta_pos[ 0 ] == 0 )
  {
    iMin = 1, iMax = 0;
  }

  // For the Y-axis
  if( delta_pos[ 1 ] > 0 )
  {
    jMin = ::ceil( ( mp_nbVox[ 1 ] + 1 ) - ( mp_halfFOV[ 1 ] - alphaMin * delta_pos[ 1 ] - event1[ 1 ] ) / mp_sizeVox[1] );
    jMax = ::floor( 1 + ( event1[ 1 ] + alphaMax * delta_pos[ 1 ] - (-mp_halfFOV[ 1 ]) ) / mp_sizeVox[1] );
  }
  else if( delta_pos[ 1 ] < 0 )
  {
    jMin = ::ceil( ( mp_nbVox[ 1 ] + 1 ) - ( mp_halfFOV[ 1 ] - alphaMax * delta_pos[ 1 ] - event1[ 1 ] ) / mp_sizeVox[1] );
    jMax = ::floor( 1 + ( event1[ 1 ] + alphaMin * delta_pos[ 1 ] - (-mp_halfFOV[ 1 ]) ) / mp_sizeVox[1] );
  }
  else if( delta_pos[ 1 ] == 0 )
  {
    jMin = 1, jMax = 0;
  }

  // For the Z-axis
  if( delta_pos[ 2 ] > 0 )
  {
    kMin = ::ceil( ( mp_nbVox[ 2 ] + 1 ) - ( mp_halfFOV[ 2 ] - alphaMin * delta_pos[ 2 ] - event1[ 2 ] ) / mp_sizeVox[2] );
    kMax = ::floor( 1 + ( event1[ 2 ] + alphaMax * delta_pos[ 2 ] - (-mp_halfFOV[ 2 ]) ) / mp_sizeVox[2] );
  }
  else if( delta_pos[ 2 ] < 0 )
  {
    kMin = ::ceil( ( mp_nbVox[ 2 ] + 1 ) - ( mp_halfFOV[ 2 ] - alphaMax * delta_pos[ 2 ] - event1[ 2 ] ) / mp_sizeVox[2] );
    kMax = ::floor( 1 + ( event1[ 2 ] + alphaMin * delta_pos[ 2 ] - (-mp_halfFOV[ 2 ]) ) / mp_sizeVox[2] );
  }
  else if( delta_pos[ 2 ] == 0 )
  {
    kMin = 1, kMax = 0;
  }

  // Computing the last term n number of intersection
  INTNB n = ( iMax - iMin + 1 ) + ( jMax - jMin + 1 )
    + ( kMax - kMin + 1 );

  // Array storing the first alpha in X, Y and Z
  double alpha_XYZ[ 3 ] = { 1.0f, 1.0f, 1.0f };

  // Computing the first alpha in X
  if( delta_pos[ 0 ] > 0 )
    alpha_XYZ[ 0 ] = ( ( (-mp_halfFOV[ 0 ]) + ( iMin - 1 ) * mp_sizeVox[0] ) - event1[ 0 ] ) / delta_pos[ 0 ];
  else if( delta_pos[ 0 ] < 0 )
    alpha_XYZ[ 0 ] = ( ( (-mp_halfFOV[ 0 ]) + ( iMax - 1 ) * mp_sizeVox[0] ) - event1[ 0 ] ) / delta_pos[ 0 ];

  // Computing the first alpha in Y
  if( delta_pos[ 1 ] > 0 )
    alpha_XYZ[ 1 ] = ( ( (-mp_halfFOV[ 1 ]) + ( jMin - 1 ) * mp_sizeVox[1] ) - event1[ 1 ] ) / delta_pos[ 1 ];
  else if( delta_pos[ 1 ] < 0 )
    alpha_XYZ[ 1 ] = ( ( (-mp_halfFOV[ 1 ]) + ( jMax - 1 ) * mp_sizeVox[1] ) - event1[ 1 ] ) / delta_pos[ 1 ];

  // Computing the first alpha in Z
  if( delta_pos[ 2 ] > 0 )
    alpha_XYZ[ 2 ] = ( ( (-mp_halfFOV[ 2 ]) + ( kMin - 1 ) * mp_sizeVox[2] ) - event1[ 2 ] ) / delta_pos[ 2 ];
  else if( delta_pos[ 2 ] < 0 )
    alpha_XYZ[ 2 ] = ( ( (-mp_halfFOV[ 2 ]) + ( kMax - 1 ) * mp_sizeVox[2] ) - event1[ 2 ] ) / delta_pos[ 2 ];

  // Computing the alpha updating
  double alpha_u[ 3 ] = {
    mp_sizeVox[0] / std::fabs( delta_pos[ 0 ] ),
    mp_sizeVox[1] / std::fabs( delta_pos[ 1 ] ),
    mp_sizeVox[2] / std::fabs( delta_pos[ 2 ] )
  };

  // Computing the index updating 
  INTNB index_u[ 3 ] = {
    (delta_pos[ 0 ] < 0) ? -1 : 1,
    (delta_pos[ 1 ] < 0) ? -1 : 1,
    (delta_pos[ 2 ] < 0) ? -1 : 1
  };

  // Check which alpha is the min/max and increment
  if( alpha_XYZ[ 0 ] == alphaMin ) alpha_XYZ[ 0 ] += alpha_u[ 0 ];
  if( alpha_XYZ[ 1 ] == alphaMin ) alpha_XYZ[ 1 ] += alpha_u[ 1 ];
  if( alpha_XYZ[ 2 ] == alphaMin ) alpha_XYZ[ 2 ] += alpha_u[ 2 ];

  // Computing the minimum value in the alpha_XYZ buffer
  double const min_alpha_XYZ = (std::min)( alpha_XYZ[ 0 ],
    (std::min)( alpha_XYZ[ 1 ], alpha_XYZ[ 2 ] ) );

  // Computing the first index of intersection
  double const alphaMid = ( min_alpha_XYZ + alphaMin ) / 2.0f;
  INTNB index_ijk[ 3 ] = {
    1 + (int)( ( event1[ 0 ] + alphaMid * delta_pos[ 0 ] - (-mp_halfFOV[ 0 ]) ) / mp_sizeVox[0] ),
    1 + (int)( ( event1[ 1 ] + alphaMid * delta_pos[ 1 ] - (-mp_halfFOV[ 1 ]) ) / mp_sizeVox[1] ),
    1 + (int)( ( event1[ 2 ] + alphaMid * delta_pos[ 2 ] - (-mp_halfFOV[ 2 ]) ) / mp_sizeVox[2] )
  };

  INTNB const w = mp_nbVox[ 0 ];
  INTNB const wh = w * mp_nbVox[ 1 ];

  // Loop over the number of plane to cross
  double alpha_c = alphaMin;
  FLTNB coeff = 0.0f;
  INTNB numVox = 0;
  for( int nP = 0; nP < n - 1; ++nP )
  {
    if( ( alpha_XYZ[ 0 ] <= alpha_XYZ[ 1 ] )
     && ( alpha_XYZ[ 0 ] <= alpha_XYZ[ 2 ] ) )
    {
      // Storing values
      if( ( alpha_XYZ[ 0 ] >= alphaMin )
       && ( index_ijk[ 0 ] - 1 >= 0 )
       && ( index_ijk[ 0 ] - 1 <= mp_nbVox[ 0 ] - 1 )
       && ( index_ijk[ 1 ] - 1 >= 0 )
       && ( index_ijk[ 1 ] - 1 <= mp_nbVox[ 1 ] - 1 )
       && ( index_ijk[ 2 ] - 1 >= 0 )
       && ( index_ijk[ 2 ] - 1 <= mp_nbVox[ 2 ] - 1 ) )
      {
        coeff = ( alpha_XYZ[ 0 ] - alpha_c ) * length_LOR;
        numVox = ( index_ijk[ 0 ] - 1 ) + ( ( index_ijk[ 1 ] - 1 ) ) * w + ( ( index_ijk[ 2 ] - 1 ) ) * wh;
        ap_ProjectionLine->AddVoxel(a_direction, numVox, coeff);
      }

      // Increment values
      alpha_c = alpha_XYZ[ 0 ];
      alpha_XYZ[ 0 ] += alpha_u[ 0 ];
      index_ijk[ 0 ] += index_u[ 0 ];
    }
    else if( ( alpha_XYZ[ 1 ] < alpha_XYZ[ 0 ] )
          && ( alpha_XYZ[ 1 ] <= alpha_XYZ[ 2 ] ) )
    {
      // Storing values
      if( ( alpha_XYZ[ 1 ] >= alphaMin )
       && ( index_ijk[ 0 ] - 1 >= 0 )
       && ( index_ijk[ 0 ] - 1 <= mp_nbVox[ 0 ] - 1 )
       && ( index_ijk[ 1 ] - 1 >= 0 )
       && ( index_ijk[ 1 ] - 1 <= mp_nbVox[ 1 ] - 1 )
       && ( index_ijk[ 2 ] - 1 >= 0 )
       && ( index_ijk[ 2 ] - 1 <= mp_nbVox[ 2 ] - 1 ) )
      {
        coeff = ( alpha_XYZ[ 1 ] - alpha_c ) * length_LOR;
        numVox = ( index_ijk[ 0 ] - 1 ) + ( ( index_ijk[ 1 ] - 1 ) ) * w + ( ( index_ijk[ 2 ] - 1 ) ) * wh;
        ap_ProjectionLine->AddVoxel(a_direction, numVox, coeff);
      }

      // Increment values
      alpha_c = alpha_XYZ[ 1 ];
      alpha_XYZ[ 1 ] += alpha_u[ 1 ];
      index_ijk[ 1 ] += index_u[ 1 ];
    }
    else if( ( alpha_XYZ[ 2 ] < alpha_XYZ[ 0 ] )
          && ( alpha_XYZ[ 2 ] < alpha_XYZ[ 1 ] ) )
    {
      // Storing values
      if( ( alpha_XYZ[ 2 ] >= alphaMin )
       && ( index_ijk[ 0 ] - 1 >= 0 )
       && ( index_ijk[ 0 ] - 1 <= mp_nbVox[ 0 ] - 1 )
       && ( index_ijk[ 1 ] - 1 >= 0 )
       && ( index_ijk[ 1 ] - 1 <= mp_nbVox[ 1 ] - 1 )
       && ( index_ijk[ 2 ] - 1 >= 0 )
       && ( index_ijk[ 2 ] - 1 <= mp_nbVox[ 2 ] - 1 ) )
      {
        coeff = ( alpha_XYZ[ 2 ] - alpha_c ) * length_LOR;
        numVox = ( index_ijk[ 0 ] - 1 ) + ( ( index_ijk[ 1 ] - 1 ) ) * w + ( ( index_ijk[ 2 ] - 1 ) ) * wh;
        ap_ProjectionLine->AddVoxel(a_direction, numVox, coeff);
      }

      // Increment values
      alpha_c = alpha_XYZ[ 2 ];
      alpha_XYZ[ 2 ] += alpha_u[ 2 ];
      index_ijk[ 2 ] += index_u[ 2 ];
    }
  }

  return 0;
}

// =====================================================================
// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
// =====================================================================

int iProjectorIncrementalSiddon::ProjectWithTOFPos(int a_direction, oProjectionLine* ap_ProjectionLine)
{
  #ifdef CASTOR_DEBUG
  if (!m_initialized)
  {
    Cerr("***** iProjectorIncrementalSiddon::ProjectWithTOFPos() -> Called while not initialized !" << endl);
    Exit(EXIT_DEBUG);
  }
  #endif

  #ifdef CASTOR_VERBOSE
  if (m_verbose>=10)
  {
    string direction = "";
    if (a_direction==FORWARD) direction = "forward";
    else direction = "backward";
    Cout("iProjectorIncrementalSiddon::Project with TOF position -> Project line '" << ap_ProjectionLine << "' in " << direction << " direction" << endl);
  }
  #endif

  // Simpler way now, hopefully it works
  FLTNB* event1Float = ap_ProjectionLine->GetPosition1();
  FLTNB* event2Float = ap_ProjectionLine->GetPosition2();
  double event1[3] = { event1Float[0], event1Float[1], event1Float[2] };
  double event2[3] = { event2Float[0], event2Float[1], event2Float[2] };

  // **************************************
  // STEP 1: LOR length calculation
  // **************************************
  double length_LOR = ap_ProjectionLine->GetLength();

  // **************************************
  // STEP 2: Compute entrance voxel indexes
  // **************************************
  double alphaFirst[] = { 0.0, 0.0, 0.0 };
  double alphaLast[] = { 0.0, 0.0, 0.0 };

  double alphaMin = 0.0f, alphaMax = 1.0f;
  double delta_pos[] = {
    event2[ 0 ] - event1[ 0 ],
    event2[ 1 ] - event1[ 1 ],
    event2[ 2 ] - event1[ 2 ]
  };

  // Get TOF info
  double tof_resolution = ap_ProjectionLine->GetTOFResolution();
  double tof_measurement = ap_ProjectionLine->GetTOFMeasurement();

  // TOF standard deviation and truncation
  double tof_sigma = tof_resolution * SPEED_OF_LIGHT * 0.5 / TWO_SQRT_TWO_LN_2;
  double tof_half_span = tof_sigma * m_TOFnbSigmas;

  // convert delta time into delta length
  double tof_deltaL = tof_measurement * SPEED_OF_LIGHT * 0.5;

  // special case for aberrant TOF measurements which belong to scattered or random counts
  // return an empty line because we know that the count does not depict the emitting object that we want to reconstruct
  if ( fabs(tof_deltaL) > length_LOR * 0.5)
  {
    return 0;
  }
  
  // distance between the first event1 and the center of the Gaussian distribution along the LOR
  double lor_tof_center = length_LOR * 0.5 + tof_deltaL;

  // coefficient for conversion from erf use to integral of actual TOF function (Gaussian with maximum=1)
  double tof_norm_coef =  tof_sigma * sqrt(2*M_PI);

  // index along each axis of the first voxel falling inside the truncated Gaussian distribution
  double tof_edge_low[] = {0,0,0};
  // index along each axis of the last voxel falling inside the truncated Gaussian distribution
  double tof_edge_high[] = {0,0,0};
  double tof_center;
  INTNB tof_index;

  // low/high voxel edges (in absolute coordinates) for truncated TOF
  for (int ax=0;ax<3;ax++)
  {
    // absolute coordinate along each axis of the center of the TOF distribution
    tof_center = event1[ax] +  lor_tof_center * delta_pos[ax] / length_LOR;

    // absolute coordinate along each axis of the lowest voxel edge spanned by the TOF distribution, limited by the lowest FOV edge
    tof_edge_low[ax] = tof_center - tof_half_span * fabs(delta_pos[ax]) / length_LOR;
    tof_index = max( (INTNB)::floor( (tof_edge_low[ax] - (-mp_halfFOV[ax])) / mp_sizeVox[ax] ), 0);
    // if low TOF edge above the highest FOV edge, return empty line
    if (tof_index>mp_nbVox[ax]-1) return 0;
    tof_edge_low[ax] = (double)tof_index *  mp_sizeVox[ax] - mp_halfFOV[ax];

    // absolute coordinate along each axis of the highest voxel edge spanned by the TOF distribution, limited by the highest FOV edge
    tof_edge_high[ax] = tof_center + tof_half_span * fabs(delta_pos[ax]) / length_LOR;
    tof_index = min( (INTNB)::floor( (tof_edge_high[ax] - (-mp_halfFOV[ax])) / mp_sizeVox[ax] ), mp_nbVox[ax]-1);
    // if high TOF edge below the lowest FOV edge, return empty line
    if (tof_index<0) return 0;
    tof_edge_high[ax] = (double)(tof_index+1) * mp_sizeVox[ax] - mp_halfFOV[ax];
  }

  // Computation of alphaMin et alphaMax values (entrance and exit point of the ray)
  for( int i = 0; i < 3; ++i )
  {
    if( delta_pos[ i ] != 0.0 )
    {
      alphaFirst[i] = (-mp_halfFOV[i] - event1[i]) / (delta_pos[ i ]);
      alphaLast[i]  = ( mp_halfFOV[i] - event1[i]) / (delta_pos[ i ]);
      alphaMin = (std::max)(alphaMin,(std::min)(alphaFirst[i],alphaLast[i]));
      alphaMax = (std::min)(alphaMax,(std::max)(alphaFirst[i],alphaLast[i]));
    }
  }

  // if alphaMax is less than or equal to alphaMin no intersection
  // and return an empty buffer
  if( alphaMax <= alphaMin ) return 0;

  // Now we have to find the indices of the particular plane
  // (iMin,iMax), (jMin,jMax), (kMin,kMax)
  int iMin = 0, iMax = 0;
  int jMin = 0, jMax = 0;
  int kMin = 0, kMax = 0;

  // For the X-axis
  if( delta_pos[ 0 ] > 0.0f )
  {
    iMin = ::ceil( ( mp_nbVox[ 0 ] + 1 ) - ( mp_halfFOV[ 0 ] - alphaMin * delta_pos[ 0 ] - event1[ 0 ] ) / mp_sizeVox[0] );
    iMax = ::floor( 1 + ( event1[ 0 ] + alphaMax * delta_pos[ 0 ] - (-mp_halfFOV[ 0 ]) ) / mp_sizeVox[0] );
  }
  else if( delta_pos[ 0 ] < 0.0f )
  {
    iMin = ::ceil( ( mp_nbVox[ 0 ] + 1 ) - ( mp_halfFOV[ 0 ] - alphaMax * delta_pos[ 0 ] - event1[ 0 ] ) / mp_sizeVox[0] );
    iMax = ::floor( 1 + ( event1[ 0 ] + alphaMin * delta_pos[ 0 ] - (-mp_halfFOV[ 0 ]) ) / mp_sizeVox[0] );
  }
  if( delta_pos[ 0 ] == 0 )
  {
    iMin = 1, iMax = 0;
  }

  // For the Y-axis
  if( delta_pos[ 1 ] > 0 )
  {
    jMin = ::ceil( ( mp_nbVox[ 1 ] + 1 ) - ( mp_halfFOV[ 1 ] - alphaMin * delta_pos[ 1 ] - event1[ 1 ] ) / mp_sizeVox[1] );
    jMax = ::floor( 1 + ( event1[ 1 ] + alphaMax * delta_pos[ 1 ] - (-mp_halfFOV[ 1 ]) ) / mp_sizeVox[1] );
  }
  else if( delta_pos[ 1 ] < 0 )
  {
    jMin = ::ceil( ( mp_nbVox[ 1 ] + 1 ) - ( mp_halfFOV[ 1 ] - alphaMax * delta_pos[ 1 ] - event1[ 1 ] ) / mp_sizeVox[1] );
    jMax = ::floor( 1 + ( event1[ 1 ] + alphaMin * delta_pos[ 1 ] - (-mp_halfFOV[ 1 ]) ) / mp_sizeVox[1] );
  }
  else if( delta_pos[ 1 ] == 0 )
  {
    jMin = 1, jMax = 0;
  }

  // For the Z-axis
  if( delta_pos[ 2 ] > 0 )
  {
    kMin = ::ceil( ( mp_nbVox[ 2 ] + 1 ) - ( mp_halfFOV[ 2 ] - alphaMin * delta_pos[ 2 ] - event1[ 2 ] ) / mp_sizeVox[2] );
    kMax = ::floor( 1 + ( event1[ 2 ] + alphaMax * delta_pos[ 2 ] - (-mp_halfFOV[ 2 ]) ) / mp_sizeVox[2] );
  }
  else if( delta_pos[ 2 ] < 0 )
  {
    kMin = ::ceil( ( mp_nbVox[ 2 ] + 1 ) - ( mp_halfFOV[ 2 ] - alphaMax * delta_pos[ 2 ] - event1[ 2 ] ) / mp_sizeVox[2] );
    kMax = ::floor( 1 + ( event1[ 2 ] + alphaMin * delta_pos[ 2 ] - (-mp_halfFOV[ 2 ]) ) / mp_sizeVox[2] );
  }
  else if( delta_pos[ 2 ] == 0 )
  {
    kMin = 1, kMax = 0;
  }

  // Computing the last term n number of intersection
  INTNB n = ( iMax - iMin + 1 ) + ( jMax - jMin + 1 )
    + ( kMax - kMin + 1 );

  // Array storing the first alpha in X, Y and Z
  double alpha_XYZ[ 3 ] = { 1.0f, 1.0f, 1.0f };

  // Computing the first alpha in X
  if( delta_pos[ 0 ] > 0 )
    alpha_XYZ[ 0 ] = ( ( (-mp_halfFOV[ 0 ]) + ( iMin - 1 ) * mp_sizeVox[0] ) - event1[ 0 ] ) / delta_pos[ 0 ];
  else if( delta_pos[ 0 ] < 0 )
    alpha_XYZ[ 0 ] = ( ( (-mp_halfFOV[ 0 ]) + ( iMax - 1 ) * mp_sizeVox[0] ) - event1[ 0 ] ) / delta_pos[ 0 ];

  // Computing the first alpha in Y
  if( delta_pos[ 1 ] > 0 )
    alpha_XYZ[ 1 ] = ( ( (-mp_halfFOV[ 1 ]) + ( jMin - 1 ) * mp_sizeVox[1] ) - event1[ 1 ] ) / delta_pos[ 1 ];
  else if( delta_pos[ 1 ] < 0 )
    alpha_XYZ[ 1 ] = ( ( (-mp_halfFOV[ 1 ]) + ( jMax - 1 ) * mp_sizeVox[1] ) - event1[ 1 ] ) / delta_pos[ 1 ];

  // Computing the first alpha in Z
  if( delta_pos[ 2 ] > 0 )
    alpha_XYZ[ 2 ] = ( ( (-mp_halfFOV[ 2 ]) + ( kMin - 1 ) * mp_sizeVox[2] ) - event1[ 2 ] ) / delta_pos[ 2 ];
  else if( delta_pos[ 2 ] < 0 )
    alpha_XYZ[ 2 ] = ( ( (-mp_halfFOV[ 2 ]) + ( kMax - 1 ) * mp_sizeVox[2] ) - event1[ 2 ] ) / delta_pos[ 2 ];

  // Computing the alpha updating
  double alpha_u[ 3 ] = {
    mp_sizeVox[0] / std::fabs( delta_pos[ 0 ] ),
    mp_sizeVox[1] / std::fabs( delta_pos[ 1 ] ),
    mp_sizeVox[2] / std::fabs( delta_pos[ 2 ] )
  };

  // Computing the index updating
  INTNB index_u[ 3 ] = {
    (delta_pos[ 0 ] < 0) ? -1 : 1,
    (delta_pos[ 1 ] < 0) ? -1 : 1,
    (delta_pos[ 2 ] < 0) ? -1 : 1
  };

  // Check which alpha is the min/max and increment
  if( alpha_XYZ[ 0 ] == alphaMin ) alpha_XYZ[ 0 ] += alpha_u[ 0 ];
  if( alpha_XYZ[ 1 ] == alphaMin ) alpha_XYZ[ 1 ] += alpha_u[ 1 ];
  if( alpha_XYZ[ 2 ] == alphaMin ) alpha_XYZ[ 2 ] += alpha_u[ 2 ];

  // Computing the minimum value in the alpha_XYZ buffer
  double const min_alpha_XYZ = (std::min)( alpha_XYZ[ 0 ], (std::min)( alpha_XYZ[ 1 ], alpha_XYZ[ 2 ] ) );

  // Computing the first index of intersection
  double const alphaMid = ( min_alpha_XYZ + alphaMin ) / 2.0f;
  INTNB index_ijk[ 3 ] = {
    1 + (INTNB)( ( event1[ 0 ] + alphaMid * delta_pos[ 0 ] - (-mp_halfFOV[ 0 ]) ) / mp_sizeVox[0] ),
    1 + (INTNB)( ( event1[ 1 ] + alphaMid * delta_pos[ 1 ] - (-mp_halfFOV[ 1 ]) ) / mp_sizeVox[1] ),
    1 + (INTNB)( ( event1[ 2 ] + alphaMid * delta_pos[ 2 ] - (-mp_halfFOV[ 2 ]) ) / mp_sizeVox[2] )
  };

  INTNB const w = mp_nbVox[ 0 ];
  INTNB const wh = w * mp_nbVox[ 1 ];

  // tof : erf of first voxel edge - Gaussian center
  double previous_edge_erf =  erf( (length_LOR * alphaMin - lor_tof_center)/ (sqrt(2.)*tof_sigma) );
  double next_edge_erf;

  // Loop over the number of plane to cross
  FLTNB coeff = 0.0f;
  INTNB numVox = 0;
  for( int nP = 0; nP < n - 1; ++nP )
  {
    if( ( alpha_XYZ[ 0 ] <= alpha_XYZ[ 1 ] )
     && ( alpha_XYZ[ 0 ] <= alpha_XYZ[ 2 ] ) )
    {
      // Storing values
      if( ( alpha_XYZ[ 0 ] >= alphaMin )
       && ( index_ijk[ 0 ] - 1 >= 0 )
       && ( index_ijk[ 0 ] - 1 <= mp_nbVox[ 0 ] - 1 )
       && ( index_ijk[ 1 ] - 1 >= 0 )
       && ( index_ijk[ 1 ] - 1 <= mp_nbVox[ 1 ] - 1 )
       && ( index_ijk[ 2 ] - 1 >= 0 )
       && ( index_ijk[ 2 ] - 1 <= mp_nbVox[ 2 ] - 1 ) )
      {
        // tof : erf of next voxel edge - Gaussian center
        next_edge_erf = erf( (length_LOR * alpha_XYZ[ 0 ] - lor_tof_center) / (sqrt(2.)*tof_sigma) );
        // integration of the Gaussian is done on the LOR portion matching the whole current voxel along X axis
        coeff = 0.5 * std::fabs(previous_edge_erf - next_edge_erf) * tof_norm_coef ;
        // keeping record of the previous edge, so as to save 1 erf computation
        previous_edge_erf = next_edge_erf;

        numVox = ( index_ijk[ 0 ] - 1 ) + ( ( index_ijk[ 1 ] - 1 ) ) * w + ( ( index_ijk[ 2 ] - 1 ) ) * wh;
        ap_ProjectionLine->AddVoxelInTOFBin(a_direction, 0, numVox, coeff);
      }

      // Increment values
      alpha_XYZ[ 0 ] += alpha_u[ 0 ];
      index_ijk[ 0 ] += index_u[ 0 ];
    }
    else if( ( alpha_XYZ[ 1 ] < alpha_XYZ[ 0 ] )
          && ( alpha_XYZ[ 1 ] <= alpha_XYZ[ 2 ] ) )
    {
      // Storing values
      if( ( alpha_XYZ[ 1 ] >= alphaMin )
       && ( index_ijk[ 0 ] - 1 >= 0 )
       && ( index_ijk[ 0 ] - 1 <= mp_nbVox[ 0 ] - 1 )
       && ( index_ijk[ 1 ] - 1 >= 0 )
       && ( index_ijk[ 1 ] - 1 <= mp_nbVox[ 1 ] - 1 )
       && ( index_ijk[ 2 ] - 1 >= 0 )
       && ( index_ijk[ 2 ] - 1 <= mp_nbVox[ 2 ] - 1 ) )
      {
        // tof : erf of next voxel edge - Gaussian center
        next_edge_erf = erf( (length_LOR * alpha_XYZ[ 1 ] - lor_tof_center) / (sqrt(2.)*tof_sigma) );
        // integration of the Gaussian is done on the LOR portion matching the whole current voxel along X axis
        coeff = 0.5 * std::fabs(previous_edge_erf - next_edge_erf) * tof_norm_coef ;
        // keeping record of the previous edge, so as to save 1 erf computation
        previous_edge_erf = next_edge_erf;

        numVox = ( index_ijk[ 0 ] - 1 ) + ( ( index_ijk[ 1 ] - 1 ) ) * w + ( ( index_ijk[ 2 ] - 1 ) ) * wh;
        ap_ProjectionLine->AddVoxelInTOFBin(a_direction, 0, numVox, coeff);
      }

      // Increment values
      alpha_XYZ[ 1 ] += alpha_u[ 1 ];
      index_ijk[ 1 ] += index_u[ 1 ];
    }
    else if( ( alpha_XYZ[ 2 ] < alpha_XYZ[ 0 ] )
          && ( alpha_XYZ[ 2 ] < alpha_XYZ[ 1 ] ) )
    {
      // Storing values
      if( ( alpha_XYZ[ 2 ] >= alphaMin )
       && ( index_ijk[ 0 ] - 1 >= 0 )
       && ( index_ijk[ 0 ] - 1 <= mp_nbVox[ 0 ] - 1 )
       && ( index_ijk[ 1 ] - 1 >= 0 )
       && ( index_ijk[ 1 ] - 1 <= mp_nbVox[ 1 ] - 1 )
       && ( index_ijk[ 2 ] - 1 >= 0 )
       && ( index_ijk[ 2 ] - 1 <= mp_nbVox[ 2 ] - 1 ) )
      {
        // tof : erf of next voxel edge - Gaussian center
        next_edge_erf = erf( (length_LOR * alpha_XYZ[ 2 ] - lor_tof_center) / (sqrt(2.)*tof_sigma) );
        // integration of the Gaussian is done on the LOR portion matching the whole current voxel along X axis
        coeff = 0.5 * std::fabs(previous_edge_erf - next_edge_erf) * tof_norm_coef ;
        // keeping record of the previous edge, so as to save 1 erf computation
        previous_edge_erf = next_edge_erf;

        numVox = ( index_ijk[ 0 ] - 1 ) + ( ( index_ijk[ 1 ] - 1 ) ) * w + ( ( index_ijk[ 2 ] - 1 ) ) * wh;
        ap_ProjectionLine->AddVoxelInTOFBin(a_direction, 0, numVox, coeff);
      }

      // Increment values
      alpha_XYZ[ 2 ] += alpha_u[ 2 ];
      index_ijk[ 2 ] += index_u[ 2 ];
    }
  }

  return 0;
}

// =====================================================================
// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
// =====================================================================

int iProjectorIncrementalSiddon::ProjectWithTOFBin(int a_direction, oProjectionLine* ap_ProjectionLine)
{
  Cerr("***** iProjectorIncrementalSiddon::ProjectWithTOFBin() -> Not yet implemented !" << endl);
  return 1;
}

// =====================================================================
// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
// =====================================================================
