/*
This file is part of CASToR.

    CASToR is free software: you can redistribute it and/or modify it under the
    terms of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option) any later
    version.

    CASToR is distributed in the hope that it will be useful, but WITHOUT ANY
    WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
    details.

    You should have received a copy of the GNU General Public License along with
    CASToR (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.

Copyright 2017 all CASToR contributors listed below:

    --> current contributors: Thibaut MERLIN, Simon STUTE, Didier BENOIT, Marina FILIPOVIC
    --> past contributors: Valentin VIELZEUF

This is CASToR version 1.2.
*/

/*
  Implementation of class iProjectorJoseph

  - separators: done
  - doxygen: done
  - default initialization: done
  - CASTOR_DEBUG: 
  - CASTOR_VERBOSE: 
*/

/*!
  \file
  \ingroup  projector
  \brief    Implementation of class iProjectorJoseph
*/

#include "iProjectorJoseph.hh"
#include "sOutputManager.hh"

#include <cmath>

// =====================================================================
// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
// =====================================================================

iProjectorJoseph::iProjectorJoseph() : vProjector()
{
  // This projector is not compatible with SPECT attenuation correction because
  // the voxels contributing to the line are not strictly ordered with respect to
  // their distance to point2 (due to interpolations at each plane crossed)
  m_compatibleWithSPECTAttenuationCorrection = false;

  // Default pointers and parameters
  mp_boundariesX = nullptr;
  mp_boundariesY = nullptr;
  mp_boundariesZ = nullptr;
  mp_maskPad = nullptr;
  m_toleranceX = 0.;
  m_toleranceY = 0.;
  m_toleranceZ = 0.;
}

// =====================================================================
// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
// =====================================================================

iProjectorJoseph::~iProjectorJoseph()
{
  if ( mp_boundariesX )
  {
    delete[] mp_boundariesX;
    mp_boundariesX = nullptr;
  }

  if ( mp_boundariesY )
  {
    delete[] mp_boundariesY;
    mp_boundariesY = nullptr;
  }

  if ( mp_boundariesZ )
  {
    delete[] mp_boundariesZ;
    mp_boundariesZ = nullptr;
  }

  if ( mp_maskPad )
  {
    delete[] mp_maskPad;
    mp_maskPad = nullptr;
  }
}

// =====================================================================
// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
// =====================================================================

int iProjectorJoseph::ReadConfigurationFile(const string& a_configurationFile)
{
  // No options for joseph
  ;
  // Normal end
  return 0;
}

// =====================================================================
// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
// =====================================================================

int iProjectorJoseph::ReadOptionsList(const string& a_optionsList)
{
  // No options for joseph
  ;
  // Normal end
  return 0;
}

// =====================================================================
// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
// =====================================================================

void iProjectorJoseph::ShowHelpSpecific()
{
  cout << "This projector is a line projector that uses linear interpolation between pixels." << endl;
  cout << "It is implemented from the following published paper:" << endl;
  cout << "P. M. Joseph, \"An improved algorithm for reprojecting rays through pixel images\", IEEE Trans. Med. Imaging, vol. 1, pp. 192-6, 1982." << endl;
}

// =====================================================================
// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
// =====================================================================

int iProjectorJoseph::CheckSpecificParameters()
{
  // Nothing to check for this projector
  ;
  // Normal end
  return 0;
}

// =====================================================================
// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
// =====================================================================

int iProjectorJoseph::InitializeSpecific()
{
  // Verbose
  if (m_verbose>=2) Cout("iProjectorJoseph::InitializeSpecific() -> Use Joseph projector" << endl);

  // Allocate and compute boundaries (grid through voxel centers)
  mp_boundariesX = new double[ mp_nbVox[ 0 ] + 2 ];
  for( INTNB i = 0; i < mp_nbVox[ 0 ] + 2; ++i ) mp_boundariesX[ i ] = -((double)(mp_halfFOV[ 0 ])) + ((double)(mp_sizeVox[ 0 ])) * ( ((double)i) - 0.5 );
  mp_boundariesY = new double[ mp_nbVox[ 1 ] + 2 ];
  for( INTNB i = 0; i < mp_nbVox[ 1 ] + 2; ++i ) mp_boundariesY[ i ] = -((double)(mp_halfFOV[ 1 ])) + ((double)(mp_sizeVox[ 1 ])) * ( ((double)i) - 0.5 );
  mp_boundariesZ = new double[ mp_nbVox[ 2 ] + 2 ];
  for( INTNB i = 0; i < mp_nbVox[ 2 ] + 2; ++i ) mp_boundariesZ[ i ] = -((double)(mp_halfFOV[ 2 ])) + ((double)(mp_sizeVox[ 2 ])) * ( ((double)i) - 0.5 );

  // Allocating the mask buffer for the padded image space
  INTNB nElts = ( mp_nbVox[ 0 ] + 2 ) * ( mp_nbVox[ 1 ] + 2 ) * ( mp_nbVox[ 2 ] + 2 );
  mp_maskPad = new uint8_t[ nElts ];
  ::memset( mp_maskPad, 0, sizeof( uint8_t ) * nElts );
  for( INTNB k = 1; k < mp_nbVox[ 2 ] + 1; ++k )
  {
    for( INTNB j = 1; j < mp_nbVox[ 1 ] + 1; ++j )
    {
      for( INTNB i = 1; i < mp_nbVox[ 0 ] + 1; ++i )
      {
        mp_maskPad[ i + j * ( ( mp_nbVox[ 0 ] + 2 ) ) + k * ( mp_nbVox[ 0 ] + 2 ) * ( mp_nbVox[ 1 ] + 2 ) ] = 1;
      }
    }
  }

  // Set the tolerance with respect to voxel sizes in each dimensions
  double tolerance_factor = 1.e-4;
  m_toleranceX = ((double)(mp_sizeVox[0])) * tolerance_factor;
  m_toleranceY = ((double)(mp_sizeVox[1])) * tolerance_factor;
  m_toleranceZ = ((double)(mp_sizeVox[2])) * tolerance_factor;

  // Normal end
  return 0;
}

// =====================================================================
// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
// =====================================================================

INTNB iProjectorJoseph::EstimateMaxNumberOfVoxelsPerLine()
{
  // Find the maximum number of voxels along a given dimension
  INTNB max_nb_voxels_in_dimension = mp_ImageDimensionsAndQuantification->GetNbVoxX();
  if (mp_ImageDimensionsAndQuantification->GetNbVoxY()>max_nb_voxels_in_dimension) max_nb_voxels_in_dimension = mp_ImageDimensionsAndQuantification->GetNbVoxY();
  if (mp_ImageDimensionsAndQuantification->GetNbVoxZ()>max_nb_voxels_in_dimension) max_nb_voxels_in_dimension = mp_ImageDimensionsAndQuantification->GetNbVoxZ();
  // We should have at most 4 voxels in a given plane, so multiply by 4
  max_nb_voxels_in_dimension *= 4;
  // Return the value
  return max_nb_voxels_in_dimension;
}

// =====================================================================
// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
// =====================================================================

int iProjectorJoseph::ProjectWithoutTOF(int a_direction, oProjectionLine* ap_ProjectionLine )
{
  #ifdef CASTOR_DEBUG
  if (!m_initialized)
  {
    Cerr("***** iProjectorJoseph::ProjectWithoutTOF() -> Called while not initialized !" << endl);
    Exit(EXIT_DEBUG);
  }
  #endif

  #ifdef CASTOR_VERBOSE
  if (m_verbose>=10)
  {
    string direction = "";
    if (a_direction==FORWARD) direction = "forward";
    else direction = "backward";
    Cout("iProjectorJoseph::Project without TOF -> Project line '" << ap_ProjectionLine << "' in " << direction << " direction" << endl);
  }
  #endif

  // Get event positions
  FLTNB* event1 = ap_ProjectionLine->GetPosition1();
  FLTNB* event2 = ap_ProjectionLine->GetPosition2();

  // Distance between point event1 and event2
  double const dX21( event2[ 0 ] - event1[ 0 ] );
  double const dY21( event2[ 1 ] - event1[ 1 ] );
  double const dZ21( event2[ 2 ] - event1[ 2 ] );

  // Compute absolute values of distances, they are used twice or more
  double const fabs_dX21 = ::fabs(dX21);
  double const fabs_dY21 = ::fabs(dY21);
  double const fabs_dZ21 = ::fabs(dZ21);

  // Size of the voxels
  double const szImg_X( mp_sizeVox[ 0 ] );
  double const szImg_Y( mp_sizeVox[ 1 ] );
  double const szImg_Z( mp_sizeVox[ 2 ] );

  // Get the number of the planes
  INTNB const nPlane_X( mp_nbVox[ 0 ] );
  INTNB const nPlane_Y( mp_nbVox[ 1 ] );
  INTNB const nPlane_Z( mp_nbVox[ 2 ] );
  INTNB const nPlane_XY( nPlane_X * nPlane_Y );

  // Get the number of the padded planes
  INTNB const nPlane_X_Pad( nPlane_X + 2 );
  INTNB const nPlane_Y_Pad( nPlane_Y + 2 );
  INTNB const nPlane_XY_Pad( nPlane_X_Pad * nPlane_Y_Pad );

  // Coordinates of the first and last planes
  double const xPlane_0 = -mp_halfFOV[ 0 ];
  double const yPlane_0 = -mp_halfFOV[ 1 ];
  double const zPlane_0 = -mp_halfFOV[ 2 ];
  double const xPlane_N = mp_halfFOV[ 0 ];
  double const yPlane_N = mp_halfFOV[ 1 ];
  double const zPlane_N = mp_halfFOV[ 2 ];

  // Parameters for the index
  INTNB index_x( 0 );
  INTNB index_y( 0 );
  INTNB index_z( 0 );

  // Parameter for the intersection point in each axis (at voxel centers)
  // X
  double intersection_x( event1[ 0 ] < xPlane_0 ?
    xPlane_0 + szImg_X / 2.0 : xPlane_N - szImg_X / 2.0 );

  // Y
  double intersection_y( event1[ 1 ] < yPlane_0 ?
    yPlane_0 + szImg_Y / 2.0 : yPlane_N - szImg_Y / 2.0 );

  // Z
  double intersection_z( event1[ 2 ] < zPlane_0 ?
    zPlane_0 + szImg_Z / 2.0 : zPlane_N - szImg_Z / 2.0 );

  // Min. Max. coordinate for histo.
  double const xPlane_0_min = xPlane_0 + szImg_X / 2.0;
  double const yPlane_0_min = yPlane_0 + szImg_Y / 2.0;
  double const zPlane_0_min = zPlane_0 + szImg_Z / 2.0;

  // Min. Max security. Do not take the last slice
  double const xPlane_0_min_safety = xPlane_0 - ( szImg_X / 2.0 );
  double const yPlane_0_min_safety = yPlane_0 - ( szImg_Y / 2.0 );
  double const zPlane_0_min_safety = zPlane_0 - ( szImg_Z / 2.0 );
  double const xPlane_N_max_safety = xPlane_N + ( szImg_X / 2.0 );
  double const yPlane_N_max_safety = yPlane_N + ( szImg_Y / 2.0 );
  double const zPlane_N_max_safety = zPlane_N + ( szImg_Z / 2.0 );

  // Incrementation of the plane
  double const increment[] = {
    event1[ 0 ] < xPlane_0 ? szImg_X : -szImg_X,
    event1[ 1 ] < yPlane_0 ? szImg_Y : -szImg_Y,
    event1[ 2 ] < zPlane_0 ? szImg_Z : -szImg_Z
  };

  INTNB const increment_index[] = {
    event1[ 0 ] < xPlane_0 ? 1 : -1,
    event1[ 1 ] < yPlane_0 ? 1 : -1,
    event1[ 2 ] < zPlane_0 ? 1 : -1
  };

  INTNB const first_index[] = {
    event1[ 0 ] < xPlane_0 ? 0 : nPlane_X - 1,
    event1[ 1 ] < yPlane_0 ? 0 : nPlane_Y - 1,
    event1[ 2 ] < zPlane_0 ? 0 : nPlane_Z - 1
  };

  // Values of interpolation
  double x1( 0.0 ); double x2( 0.0 );
  double y1( 0.0 ); double y2( 0.0 );
  double z1( 0.0 ); double z2( 0.0 );

  // Find the principal direction
  // X direction
  if( fabs_dX21 >= fabs_dY21 && fabs_dX21 >= fabs_dZ21 )
  {
    // Compute the weight to normalize the line
    double const weight( ((double)(ap_ProjectionLine->GetLength())) * szImg_X / fabs_dX21 );

    // Take the increment X
    double const incrementX = increment[ 0 ];

    // Compute first intersection in Y and Z
    double const p1X_IntersectionX1 = ( intersection_x - event1[ 0 ] );
    intersection_y = ( p1X_IntersectionX1 * dY21 / dX21 ) + event1[ 1 ];
    intersection_z = ( p1X_IntersectionX1 * dZ21 / dX21 ) + event1[ 2 ];

    // Compute the increment (next intersection minus current intersection)
    double const incrementY =
      ( ( intersection_x + incrementX - event1[ 0 ] ) * dY21 / dX21 )
      + event1[ 1 ] - intersection_y;
    double const incrementZ =
      ( ( intersection_x + incrementX - event1[ 0 ] ) * dZ21 / dX21 )
      + event1[ 2 ] - intersection_z;

    // Take first index for x
    index_x = first_index[ 0 ];

    // Loop over the Y-Z planes
    for( INTNB i = 0; i < nPlane_X; ++i )
    {
      // Check if intersection outside of the image space
      if( intersection_y < ( yPlane_0_min_safety + m_toleranceY )
        || intersection_y >= ( yPlane_N_max_safety - m_toleranceY )
        || intersection_z < ( zPlane_0_min_safety + m_toleranceZ )
        || intersection_z >= ( zPlane_N_max_safety - m_toleranceZ ) )
      {
        index_x += increment_index[ 0 ];
        intersection_y += incrementY;
        intersection_z += incrementZ;
        continue;
      }

      // Find the index in the image
      index_y = ::floor( ( intersection_y - yPlane_0_min ) / szImg_Y );
      index_z = ::floor( ( intersection_z - zPlane_0_min ) / szImg_Z );

      // Compute distance between intersection point and Y and Z boundaries
      y2 = ( intersection_y - mp_boundariesY[ index_y + 1 ] ) / szImg_Y;
      y1 = 1.0 - y2;
      z2 = ( intersection_z - mp_boundariesZ[ index_z + 1 ] ) / szImg_Z;
      z1 = 1.0 - z2;

      // Index in the image space
      INTNB index_final = index_x + index_y * nPlane_X + index_z * nPlane_XY;

      // Index in the padded image space
      INTNB index_final_pad = ( index_x + 1 ) + ( index_y + 1 ) * nPlane_X_Pad + ( index_z + 1 ) * nPlane_XY_Pad;
      INTNB maskIdx1 = mp_maskPad[ index_final_pad ];
      INTNB maskIdx2 = mp_maskPad[ index_final_pad + nPlane_X_Pad ];
      INTNB maskIdx3 = mp_maskPad[ index_final_pad + nPlane_XY_Pad ];
      INTNB maskIdx4 = mp_maskPad[ index_final_pad + nPlane_X_Pad + nPlane_XY_Pad ];

      ap_ProjectionLine->AddVoxel(a_direction, ( index_final ) * maskIdx1, ( y1 * z1 ) * weight * maskIdx1);
      ap_ProjectionLine->AddVoxel(a_direction, ( index_final + nPlane_X ) * maskIdx2, ( y2 * z1 ) * weight * maskIdx2);
      ap_ProjectionLine->AddVoxel(a_direction, ( index_final + nPlane_XY ) * maskIdx3, ( y1 * z2 ) * weight * maskIdx3);
      ap_ProjectionLine->AddVoxel(a_direction, ( index_final + nPlane_X + nPlane_XY ) * maskIdx4, ( y2 * z2 ) * weight * maskIdx4);

      // Increment
      index_x += increment_index[ 0 ];
      intersection_y += incrementY;
      intersection_z += incrementZ;
    }
  } // Y direction
  else if( fabs_dY21 > fabs_dX21 && fabs_dY21 >= fabs_dZ21 )
  {
    // Compute the weight to normalize the line
    double const weight( ((double)(ap_ProjectionLine->GetLength())) * szImg_Y / fabs_dY21 );

    // Take the increment Y
    double const incrementY = increment[ 1 ];

    // Compute first intersection in X and Z
    double const p1Y_IntersectionY1 = ( intersection_y - event1[ 1 ] );
    intersection_x = ( p1Y_IntersectionY1 * dX21 / dY21 ) + event1[ 0 ];
    intersection_z = ( p1Y_IntersectionY1 * dZ21 / dY21 ) + event1[ 2 ];

    // Compute the increment (next intersection minus current intersection)
    double const incrementX =
      ( ( intersection_y + incrementY - event1[ 1 ] ) * dX21 / dY21 )
      + event1[ 0 ] - intersection_x;
    double const incrementZ =
      ( ( intersection_y + incrementY - event1[ 1 ] ) * dZ21 / dY21 )
      + event1[ 2 ] - intersection_z;

    // Take first index for y
    index_y = first_index[ 1 ];

    // Loop over the X-Z planes
    for( INTNB i = 0; i < nPlane_Y; ++i )
    {
      // Check if intersection outside of the image space
      if( intersection_x < ( xPlane_0_min_safety + m_toleranceX )
        || intersection_x >= ( xPlane_N_max_safety - m_toleranceX )
        || intersection_z < ( zPlane_0_min_safety + m_toleranceZ )
        || intersection_z >= ( zPlane_N_max_safety - m_toleranceZ ) )
      {
        index_y += increment_index[ 1 ];
        intersection_x += incrementX;
        intersection_z += incrementZ;
        continue;
      }

      // Find the index in the image
      index_x = ::floor( ( intersection_x - xPlane_0_min ) / szImg_X );
      index_z = ::floor( ( intersection_z - zPlane_0_min ) / szImg_Z );

      // Compute distance between intersection point and Y and Z boundaries
      x2 = ( intersection_x - mp_boundariesX[ index_x + 1 ] ) / szImg_X;
      x1 = 1.0 - x2;
      z2 = ( intersection_z - mp_boundariesZ[ index_z + 1 ] ) / szImg_Z;
      z1 = 1.0 - z2;

      // Index in the image space
      uint32_t index_final = index_x + index_y * nPlane_X + index_z * nPlane_XY;

      // Index in the padded image space
      INTNB index_final_pad = ( index_x + 1 ) + ( index_y + 1 ) * nPlane_X_Pad + ( index_z + 1 ) * nPlane_XY_Pad;
      INTNB maskIdx1 = mp_maskPad[ index_final_pad ];
      INTNB maskIdx2 = mp_maskPad[ index_final_pad + 1 ];
      INTNB maskIdx3 = mp_maskPad[ index_final_pad + nPlane_XY_Pad ];
      INTNB maskIdx4 = mp_maskPad[ index_final_pad + 1 + nPlane_XY_Pad ];

      ap_ProjectionLine->AddVoxel(a_direction, ( index_final ) * maskIdx1, ( x1 * z1 ) * weight * maskIdx1);
      ap_ProjectionLine->AddVoxel(a_direction, ( index_final + 1 ) * maskIdx2, ( x2 * z1 ) * weight * maskIdx2);
      ap_ProjectionLine->AddVoxel(a_direction, ( index_final + nPlane_XY ) * maskIdx3, ( x1 * z2 ) * weight * maskIdx3);
      ap_ProjectionLine->AddVoxel(a_direction, ( index_final + 1 + nPlane_XY ) * maskIdx4, ( x2 * z2 ) * weight * maskIdx4);

      // Increment
      index_y += increment_index[ 1 ];
      intersection_x += incrementX;
      intersection_z += incrementZ;
    }
  }
  else // Z direction
  {
    // Compute the weight to normalize the line
    double const weight( ((double)(ap_ProjectionLine->GetLength())) * szImg_Z / fabs_dZ21 );

    // Take the increment Z
    double const incrementZ = increment[ 2 ];

    // Compute first intersection in X and Y
    double const p1Z_IntersectionZ1 = ( intersection_z - event1[ 2 ] );
    intersection_y = ( p1Z_IntersectionZ1 * dY21 / dZ21 ) + event1[ 1 ];
    intersection_x = ( p1Z_IntersectionZ1 * dX21 / dZ21 ) + event1[ 0 ];

    // Compute the increment (next intersection minus current intersection)
    double const incrementY =
      ( ( intersection_z + incrementZ - event1[ 2 ] ) * dY21 / dZ21 )
      + event1[ 1 ] - intersection_y;
    double const incrementX =
      ( ( intersection_z + incrementZ - event1[ 2 ] ) * dX21 / dZ21 )
      + event1[ 0 ] - intersection_x;

    // Take first index for z
    index_z = first_index[ 2 ];

    // Loop over the Y-Z planes
    for( INTNB i = 0; i < nPlane_Z; ++i )
    {
      // Check if intersection outside of the image space
      if( intersection_y < ( yPlane_0_min_safety + m_toleranceY )
        || intersection_y >= ( yPlane_N_max_safety - m_toleranceY )
        || intersection_x < ( xPlane_0_min_safety + m_toleranceX )
        || intersection_x >= ( xPlane_N_max_safety - m_toleranceX ) )
      {
        index_z += increment_index[ 2 ];
        intersection_y += incrementY;
        intersection_x += incrementX;
        continue;
      }

      // Find the index in the image
      index_y = ::floor( ( intersection_y - yPlane_0_min ) / szImg_Y );
      index_x = ::floor( ( intersection_x - xPlane_0_min ) / szImg_X );

      // Compute distance between intersection point and Y and Z boundaries
      y2 = ( intersection_y - mp_boundariesY[ index_y + 1 ] ) / szImg_Y;
      y1 = 1.0 - y2;
      x2 = ( intersection_x - mp_boundariesX[ index_x + 1 ] ) / szImg_X;
      x1 = 1.0 - x2;

      // Index in the image space
      uint32_t index_final = index_x + index_y * nPlane_X + index_z * nPlane_XY;

      // Index in the padded image space
      INTNB index_final_pad = ( index_x + 1 ) + ( index_y + 1 ) * nPlane_X_Pad + ( index_z + 1 ) * nPlane_XY_Pad;
      INTNB maskIdx1 = mp_maskPad[ index_final_pad ];
      INTNB maskIdx2 = mp_maskPad[ index_final_pad + 1 ];
      INTNB maskIdx3 = mp_maskPad[ index_final_pad + nPlane_X_Pad ];
      INTNB maskIdx4 = mp_maskPad[ index_final_pad + 1 + nPlane_X_Pad ];

      ap_ProjectionLine->AddVoxel(a_direction, ( index_final ) * maskIdx1, ( x1 * y1 ) * weight * maskIdx1);
      ap_ProjectionLine->AddVoxel(a_direction, ( index_final + 1 ) * maskIdx2, ( x2 * y1 ) * weight * maskIdx2);
      ap_ProjectionLine->AddVoxel(a_direction, ( index_final + nPlane_X ) * maskIdx3, ( x1 * y2 ) * weight * maskIdx3);
      ap_ProjectionLine->AddVoxel(a_direction, ( index_final + nPlane_X + 1 ) * maskIdx4, ( x2 * y2 ) * weight * maskIdx4);

      // Increment
      index_z += increment_index[ 2 ];
      intersection_y += incrementY;
      intersection_x += incrementX;
    }
  }
  return 0;
}

// =====================================================================
// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
// =====================================================================





int iProjectorJoseph::ProjectWithTOFPos(int a_direction, oProjectionLine* ap_ProjectionLine)
{
  #ifdef CASTOR_DEBUG
  if (!m_initialized)
  {
    Cerr("***** iProjectorJoseph::ProjectWithTOFPos() -> Called while not initialized !" << endl);
    Exit(EXIT_DEBUG);
  }
  #endif

  #ifdef CASTOR_VERBOSE
  if (m_verbose>=10)
  {
    string direction = "";
    if (a_direction==FORWARD) direction = "forward";
    else direction = "backward";
    Cout("iProjectorJoseph::Project with TOF pos -> Project line '" << ap_ProjectionLine << "' in " << direction << " direction" << endl);
  }
  #endif

  // Get event positions
  FLTNB* event1 = ap_ProjectionLine->GetPosition1();
  FLTNB* event2 = ap_ProjectionLine->GetPosition2();
  
  // LOR length
  double lor_length = ap_ProjectionLine->GetLength();

  // Get TOF info
  FLTNB tof_resolution = ap_ProjectionLine->GetTOFResolution();
  FLTNB tof_measurement = ap_ProjectionLine->GetTOFMeasurement();

  // TOF standard deviation and truncation
  double tof_sigma = tof_resolution * SPEED_OF_LIGHT * 0.5 / TWO_SQRT_TWO_LN_2;
  double tof_half_span = tof_sigma * m_TOFnbSigmas;

  // convert delta time into delta length
  double tof_deltaL = tof_measurement * SPEED_OF_LIGHT * 0.5;
  
  // special case for aberrant TOF measurements which must belong to scattered or random counts
  // return an empty line because we know that the count does not depict the emitting object that we want to reconstruct
  if ( fabs(tof_deltaL) > lor_length * 0.5)
  {
    return 0;
  }

  // distance between the first event and the center of the Gaussian distribution along the LOR
  double lor_tof_center = lor_length * 0.5 + tof_deltaL;

  // coefficient for conversion from erf use to integral of actual TOF function (Gaussian with maximum=1)
  double tof_norm_coef =  tof_sigma * sqrt(2*M_PI);

  // Distance between point event1 and event2
  double const dX21( event2[ 0 ] - event1[ 0 ] );
  double const dY21( event2[ 1 ] - event1[ 1 ] );
  double const dZ21( event2[ 2 ] - event1[ 2 ] );

  // Compute absolute values of distances, they are used twice or more
  double const fabs_dX21 = ::fabs(dX21);
  double const fabs_dY21 = ::fabs(dY21);
  double const fabs_dZ21 = ::fabs(dZ21);

  // Size of the voxels
  double const szImg_X( mp_sizeVox[ 0 ] );
  double const szImg_Y( mp_sizeVox[ 1 ] );
  double const szImg_Z( mp_sizeVox[ 2 ] );

  // Get the number of the planes
  INTNB const nPlane_X( mp_nbVox[ 0 ] );
  INTNB const nPlane_Y( mp_nbVox[ 1 ] );
  INTNB const nPlane_Z( mp_nbVox[ 2 ] );
  INTNB const nPlane_XY( nPlane_X * nPlane_Y );

  // Get the number of the padded planes
  INTNB const nPlane_X_Pad( nPlane_X + 2 );
  INTNB const nPlane_Y_Pad( nPlane_Y + 2 );
  INTNB const nPlane_XY_Pad( nPlane_X_Pad * nPlane_Y_Pad );

  // Coordinates of the first and last planes (passing through voxel edges)
  double const xPlane_0 = -mp_halfFOV[ 0 ];
  double const yPlane_0 = -mp_halfFOV[ 1 ];
  double const zPlane_0 = -mp_halfFOV[ 2 ];
  double const xPlane_N = mp_halfFOV[ 0 ];
  double const yPlane_N = mp_halfFOV[ 1 ];
  double const zPlane_N = mp_halfFOV[ 2 ];

  // Parameters for the index
  INTNB index_x( 0 );
  INTNB index_y( 0 );
  INTNB index_z( 0 );

  // coordinates of the center of the Gaussian distribution (signed distances manage the order of event1/event2 along each axis)
  double tof_center[] = {0,0,0};
  // coordinates of the lower edge of the first voxel falling inside the truncated Gaussian distribution
  double tof_edge_low[] = {0,0,0};
  // coordinate of the upper edge of the last voxel falling inside the truncated Gaussian distribution
  double tof_edge_high[] = {0,0,0};
  // index along each axis of the first voxel falling inside the truncated Gaussian distribution
  INTNB tof_index_low[] = {0,0,0};
  // index along each axis of the last voxel falling inside the truncated Gaussian distribution
  INTNB tof_index_high[] = {0,0,0};

  // TOF X
  tof_center[0] = event1[ 0 ] +  lor_tof_center * dX21 / lor_length;
  
  tof_edge_low[0] = tof_center[0] - tof_half_span * fabs_dX21 / lor_length;
  // make the coordinate match the lowest voxel edge
  tof_index_low[0] = max( (INTNB)::floor( (tof_edge_low[0] - xPlane_0) / szImg_X ), 0);
  // if TOF outside of field of view, return empty line
  if (tof_index_low[0]>nPlane_X-1) return 0;
  tof_edge_low[0] = (double)tof_index_low[0] * szImg_X + xPlane_0;

  tof_edge_high[0] = tof_center[0] + tof_half_span * fabs_dX21 / lor_length;
  // make the coordinate match the highest voxel edge
  tof_index_high[0] = min( (INTNB)::floor( (tof_edge_high[0] - xPlane_0) / szImg_X ), nPlane_X-1);
  // if TOF outside of field of view, return empty line
  if (tof_index_high[0]<0) return 0;
  tof_edge_high[0] = (double)(tof_index_high[0]+1) * szImg_X + xPlane_0;

  // TOF Y
  tof_center[1] = event1[ 1 ] +  lor_tof_center * dY21 / lor_length;

  tof_edge_low[1] = tof_center[1] - tof_half_span * fabs_dY21 / lor_length;
  tof_index_low[1] = max( (INTNB)::floor( (tof_edge_low[1] - yPlane_0) / szImg_Y ), 0);
  if (tof_index_low[1]>nPlane_Y-1) return 0;
  tof_edge_low[1] = (double)tof_index_low[1] * szImg_Y + yPlane_0;

  tof_edge_high[1] = tof_center[1] + tof_half_span * fabs_dY21 / lor_length;
  tof_index_high[1] = min( (INTNB)::floor( (tof_edge_high[1] - yPlane_0) / szImg_Y ), nPlane_Y-1);
  if (tof_index_high[1]<0) return 0;
  tof_edge_high[1] = (double)(tof_index_high[1]+1) * szImg_Y + yPlane_0;

  // TOF Z
  tof_center[2] = event1[ 2 ] +  lor_tof_center * dZ21 / lor_length;

  tof_edge_low[2] = tof_center[2] - tof_half_span * fabs_dZ21 / lor_length;
  tof_index_low[2] = max( (INTNB)::floor( (tof_edge_low[2] - zPlane_0) / szImg_Z ), 0);
  if (tof_index_low[2]>nPlane_Z-1) return 0;
  tof_edge_low[2] = (double)tof_index_low[2] * szImg_Z + zPlane_0;

  tof_edge_high[2] = tof_center[2] + tof_half_span * fabs_dZ21 / lor_length;
  tof_index_high[2] = min( (INTNB)::floor( (tof_edge_high[2] - zPlane_0) / szImg_Z ), nPlane_Z-1);
  if (tof_index_high[2]<0) return 0;
  tof_edge_high[2] = (double)(tof_index_high[2]+1) * szImg_Z + zPlane_0;

  // Parameter for the intersection point in each axis (intersection with voxel centers)
  // X
  double intersection_x( event1[ 0 ] < xPlane_0 ?
    tof_edge_low[0] + szImg_X / 2.0 : tof_edge_high[0] - szImg_X / 2.0 );

  // Y
  double intersection_y( event1[ 1 ] < yPlane_0 ?
    tof_edge_low[1] + szImg_Y / 2.0 : tof_edge_high[1] - szImg_Y / 2.0 );

  // Z
  double intersection_z( event1[ 2 ] < zPlane_0 ?
    tof_edge_low[2] + szImg_Z / 2.0 : tof_edge_high[2] - szImg_Z / 2.0 );

  // Min. Max. coordinate for histo.
  double const xPlane_0_min = xPlane_0 + szImg_X / 2.0;
  double const yPlane_0_min = yPlane_0 + szImg_Y / 2.0;
  double const zPlane_0_min = zPlane_0 + szImg_Z / 2.0;

  // Min. Max security. Do not take the last slice
  double const xPlane_0_min_safety = xPlane_0 - ( szImg_X / 2.0 );
  double const yPlane_0_min_safety = yPlane_0 - ( szImg_Y / 2.0 );
  double const zPlane_0_min_safety = zPlane_0 - ( szImg_Z / 2.0 );
  double const xPlane_N_max_safety = xPlane_N + ( szImg_X / 2.0 );
  double const yPlane_N_max_safety = yPlane_N + ( szImg_Y / 2.0 );
  double const zPlane_N_max_safety = zPlane_N + ( szImg_Z / 2.0 );

  // Incrementation of the plane
  double const increment[] = {
    event1[ 0 ] < xPlane_0 ? szImg_X : -szImg_X,
    event1[ 1 ] < yPlane_0 ? szImg_Y : -szImg_Y,
    event1[ 2 ] < zPlane_0 ? szImg_Z : -szImg_Z
  };

  INTNB const increment_index[] = {
    event1[ 0 ] < xPlane_0 ? 1 : -1,
    event1[ 1 ] < yPlane_0 ? 1 : -1,
    event1[ 2 ] < zPlane_0 ? 1 : -1
  };

  INTNB const first_index[] = {
    event1[ 0 ] < xPlane_0 ? (INTNB)tof_index_low[0] : (INTNB)tof_index_high[0],
    event1[ 1 ] < yPlane_0 ? (INTNB)tof_index_low[1] : (INTNB)tof_index_high[1],
    event1[ 2 ] < zPlane_0 ? (INTNB)tof_index_low[2] : (INTNB)tof_index_high[2],
  };


  // Values of interpolation
  double x1( 0.0 ); double x2( 0.0 );
  double y1( 0.0 ); double y2( 0.0 );
  double z1( 0.0 ); double z2( 0.0 );

  // Find the principal direction
  // X direction
  if( fabs_dX21 >= fabs_dY21 && fabs_dX21 >= fabs_dZ21 )
  {
    // Take the increment X
    double const incrementX = increment[ 0 ];

    // Compute first intersection in Y and Z
    double const p1X_IntersectionX1 = ( intersection_x - event1[ 0 ] );
    intersection_y = ( p1X_IntersectionX1 * dY21 / dX21 ) + event1[ 1 ];
    intersection_z = ( p1X_IntersectionX1 * dZ21 / dX21 ) + event1[ 2 ];

    // Compute the increment (next intersection minus current intersection)
    double const incrementY =
      ( ( intersection_x + incrementX - event1[ 0 ] ) * dY21 / dX21 )
      + event1[ 1 ] - intersection_y;
    double const incrementZ =
      ( ( intersection_x + incrementX - event1[ 0 ] ) * dZ21 / dX21 )
      + event1[ 2 ] - intersection_z;

    // Take first index for x
    index_x = first_index[ 0 ];

    // tof : erf of previous voxel edge - Gaussian center
    double previous_edge_erf =  erf( ( ( intersection_x - increment_index[ 0 ] * 0.5 *szImg_X - tof_center[0])  * lor_length / fabs_dX21 ) / ( sqrt(2.)*tof_sigma ) );
    double next_edge_erf, tof_weight;
    
    // Loop over the Y-Z planes
    for( INTNB i = 0; i < (INTNB)(tof_index_high[0] - tof_index_low[0] + 1); ++i )
    {
      // Check if intersection outside of the image space
      if( intersection_y < ( yPlane_0_min_safety + m_toleranceY )
        || intersection_y >= ( yPlane_N_max_safety - m_toleranceY )
        || intersection_z < ( zPlane_0_min_safety + m_toleranceZ )
        || intersection_z >= ( zPlane_N_max_safety - m_toleranceZ ) )
      {
        index_x += increment_index[ 0 ];
        intersection_x += incrementX;
        intersection_y += incrementY;
        intersection_z += incrementZ;
        continue;
      }

      // Find the index in the image
      index_y = ::floor( ( intersection_y - yPlane_0_min ) / szImg_Y );
      index_z = ::floor( ( intersection_z - zPlane_0_min ) / szImg_Z );

      // Compute distance between intersection point and Y and Z boundaries
      y2 = ( intersection_y - mp_boundariesY[ index_y + 1 ] ) / szImg_Y;
      y1 = 1.0 - y2;
      z2 = ( intersection_z - mp_boundariesZ[ index_z + 1 ] ) / szImg_Z;
      z1 = 1.0 - z2;

      // Index in the image space
      INTNB index_final = index_x + index_y * nPlane_X + index_z * nPlane_XY;

      // Index in the padded image space
      INTNB index_final_pad = ( index_x + 1 ) + ( index_y + 1 ) * nPlane_X_Pad + ( index_z + 1 ) * nPlane_XY_Pad;
      INTNB maskIdx1 = mp_maskPad[ index_final_pad ];
      INTNB maskIdx2 = mp_maskPad[ index_final_pad + nPlane_X_Pad ];
      INTNB maskIdx3 = mp_maskPad[ index_final_pad + nPlane_XY_Pad ];
      INTNB maskIdx4 = mp_maskPad[ index_final_pad + nPlane_X_Pad + nPlane_XY_Pad ];

      // tof : erf of next voxel edge - Gaussian center
      next_edge_erf = erf( ( ( intersection_x + increment_index[ 0 ] * 0.5 * szImg_X - tof_center[0]  ) * lor_length / fabs_dX21 ) / (sqrt(2.)*tof_sigma) );
      // integration of the Gaussian is done on the LOR portion matching the whole current voxel along X axis
      tof_weight = 0.5 * fabs(previous_edge_erf - next_edge_erf) * tof_norm_coef ;
      // keeping record of the previous edge, so as to save 1 erf computation
      previous_edge_erf = next_edge_erf;

      ap_ProjectionLine->AddVoxelInTOFBin(a_direction, 0, ( index_final ) * maskIdx1, ( y1 * z1 ) * tof_weight * maskIdx1);
      ap_ProjectionLine->AddVoxelInTOFBin(a_direction, 0, ( index_final + nPlane_X ) * maskIdx2, ( y2 * z1 ) * tof_weight * maskIdx2);
      ap_ProjectionLine->AddVoxelInTOFBin(a_direction, 0, ( index_final + nPlane_XY ) * maskIdx3, ( y1 * z2 ) * tof_weight * maskIdx3);
      ap_ProjectionLine->AddVoxelInTOFBin(a_direction, 0, ( index_final + nPlane_X + nPlane_XY ) * maskIdx4, ( y2 * z2 ) * tof_weight * maskIdx4);

      // Increment
      index_x += increment_index[ 0 ];
      intersection_x += incrementX;
      intersection_y += incrementY;
      intersection_z += incrementZ;
    }
  } // Y direction
  else if( fabs_dY21 > fabs_dX21 && fabs_dY21 >= fabs_dZ21 )
  {
    // Take the increment Y
    double const incrementY = increment[ 1 ];

    // Compute first intersection in X and Z
    double const p1Y_IntersectionY1 = ( intersection_y - event1[ 1 ] );
    intersection_x = ( p1Y_IntersectionY1 * dX21 / dY21 ) + event1[ 0 ];
    intersection_z = ( p1Y_IntersectionY1 * dZ21 / dY21 ) + event1[ 2 ];

    // Compute the increment (next intersection minus current intersection)
    double const incrementX =
      ( ( intersection_y + incrementY - event1[ 1 ] ) * dX21 / dY21 )
      + event1[ 0 ] - intersection_x;
    double const incrementZ =
      ( ( intersection_y + incrementY - event1[ 1 ] ) * dZ21 / dY21 )
      + event1[ 2 ] - intersection_z;

    // Take first index for y
    index_y = first_index[ 1 ];

    // tof : erf of previous voxel edge - Gaussian center
    double previous_edge_erf =  erf( ( ( intersection_y - increment_index[ 1 ] * 0.5 *szImg_Y - tof_center[1]  ) * lor_length / fabs_dY21) / ( sqrt(2.)*tof_sigma ) );
    double next_edge_erf, tof_weight;
    
    // Loop over the X-Z planes
    for( INTNB i = 0; i < (INTNB)(tof_index_high[1] - tof_index_low[1] + 1); ++i )
    {
      // Check if intersection outside of the image space
      if( intersection_x < ( xPlane_0_min_safety + m_toleranceX )
        || intersection_x >= ( xPlane_N_max_safety - m_toleranceX )
        || intersection_z < ( zPlane_0_min_safety + m_toleranceZ )
        || intersection_z >= ( zPlane_N_max_safety - m_toleranceZ ) )
      {
        index_y += increment_index[ 1 ];
        intersection_y += incrementY;
        intersection_x += incrementX;
        intersection_z += incrementZ;
        continue;
      }

      // Find the index in the image
      index_x = ::floor( ( intersection_x - xPlane_0_min ) / szImg_X );
      index_z = ::floor( ( intersection_z - zPlane_0_min ) / szImg_Z );

      // Compute distance between intersection point and Y and Z boundaries
      x2 = ( intersection_x - mp_boundariesX[ index_x + 1 ] ) / szImg_X;
      x1 = 1.0 - x2;
      z2 = ( intersection_z - mp_boundariesZ[ index_z + 1 ] ) / szImg_Z;
      z1 = 1.0 - z2;

      // Index in the image space
      uint32_t index_final = index_x + index_y * nPlane_X + index_z * nPlane_XY;

      // Index in the padded image space
      INTNB index_final_pad = ( index_x + 1 ) + ( index_y + 1 ) * nPlane_X_Pad + ( index_z + 1 ) * nPlane_XY_Pad;
      INTNB maskIdx1 = mp_maskPad[ index_final_pad ];
      INTNB maskIdx2 = mp_maskPad[ index_final_pad + 1 ];
      INTNB maskIdx3 = mp_maskPad[ index_final_pad + nPlane_XY_Pad ];
      INTNB maskIdx4 = mp_maskPad[ index_final_pad + 1 + nPlane_XY_Pad ];

      // tof : erf of next voxel edge - Gaussian center
      next_edge_erf = erf( ( ( intersection_y + increment_index[ 1 ] * 0.5 * szImg_Y - tof_center[1]  ) * lor_length / fabs_dY21 ) / (sqrt(2.)*tof_sigma) );
      // integration of the Gaussian is done on the LOR portion matching the whole current voxel along X axis
      tof_weight = 0.5 * fabs(previous_edge_erf - next_edge_erf) * tof_norm_coef;
      // keeping record of the previous edge, so as to save 1 erf computation
      previous_edge_erf = next_edge_erf;

      ap_ProjectionLine->AddVoxelInTOFBin(a_direction, 0, ( index_final ) * maskIdx1, ( x1 * z1 ) * tof_weight * maskIdx1);
      ap_ProjectionLine->AddVoxelInTOFBin(a_direction, 0, ( index_final + 1 ) * maskIdx2, ( x2 * z1 ) * tof_weight * maskIdx2);
      ap_ProjectionLine->AddVoxelInTOFBin(a_direction, 0, ( index_final + nPlane_XY ) * maskIdx3, ( x1 * z2 ) * tof_weight * maskIdx3);
      ap_ProjectionLine->AddVoxelInTOFBin(a_direction, 0, ( index_final + 1 + nPlane_XY ) * maskIdx4, ( x2 * z2 ) * tof_weight * maskIdx4);

      // Increment
      index_y += increment_index[ 1 ];
      intersection_y += incrementY;
      intersection_x += incrementX;
      intersection_z += incrementZ;
    }
  }
  else // Z direction
  {
    // Take the increment Z
    double const incrementZ = increment[ 2 ];

    // Compute first intersection in X and Y
    double const p1Z_IntersectionZ1 = ( intersection_z - event1[ 2 ] );
    intersection_y = ( p1Z_IntersectionZ1 * dY21 / dZ21 ) + event1[ 1 ];
    intersection_x = ( p1Z_IntersectionZ1 * dX21 / dZ21 ) + event1[ 0 ];

    // Compute the increment (next intersection minus current intersection)
    double const incrementY =
      ( ( intersection_z + incrementZ - event1[ 2 ] ) * dY21 / dZ21 )
      + event1[ 1 ] - intersection_y;
    double const incrementX =
      ( ( intersection_z + incrementZ - event1[ 2 ] ) * dX21 / dZ21 )
      + event1[ 0 ] - intersection_x;

    // Take first index for z
    index_z = first_index[ 2 ];

    // tof : erf of previous voxel edge - Gaussian center
    double previous_edge_erf =  erf( ( ( intersection_z - increment_index[ 2 ] * 0.5 *szImg_Z - tof_center[2]  ) * lor_length / fabs_dZ21) / ( sqrt(2.)*tof_sigma ) );
    double next_edge_erf, tof_weight;
    
    // Loop over the Y-Z planes
    for( INTNB i = 0; i < (INTNB)(tof_index_high[2] - tof_index_low[2] + 1); ++i )
    {
      // Check if intersection outside of the image space
      if( intersection_y < ( yPlane_0_min_safety + m_toleranceY )
        || intersection_y >= ( yPlane_N_max_safety - m_toleranceY )
        || intersection_x < ( xPlane_0_min_safety + m_toleranceX )
        || intersection_x >= ( xPlane_N_max_safety - m_toleranceX ) )
      {
        index_z += increment_index[ 2 ];
        intersection_z += incrementZ;
        intersection_y += incrementY;
        intersection_x += incrementX;
        continue;
      }

      // Find the index in the image
      index_y = ::floor( ( intersection_y - yPlane_0_min ) / szImg_Y );
      index_x = ::floor( ( intersection_x - xPlane_0_min ) / szImg_X );

      // Compute distance between intersection point and Y and Z boundaries
      y2 = ( intersection_y - mp_boundariesY[ index_y + 1 ] ) / szImg_Y;
      y1 = 1.0 - y2;
      x2 = ( intersection_x - mp_boundariesX[ index_x + 1 ] ) / szImg_X;
      x1 = 1.0 - x2;

      // Index in the image space
      uint32_t index_final = index_x + index_y * nPlane_X + index_z * nPlane_XY;

      // Index in the padded image space
      INTNB index_final_pad = ( index_x + 1 ) + ( index_y + 1 ) * nPlane_X_Pad + ( index_z + 1 ) * nPlane_XY_Pad;
      INTNB maskIdx1 = mp_maskPad[ index_final_pad ];
      INTNB maskIdx2 = mp_maskPad[ index_final_pad + 1 ];
      INTNB maskIdx3 = mp_maskPad[ index_final_pad + nPlane_X_Pad ];
      INTNB maskIdx4 = mp_maskPad[ index_final_pad + 1 + nPlane_X_Pad ];

      // tof : erf of next voxel edge - Gaussian center
      next_edge_erf = erf( ( ( intersection_z + increment_index[ 2 ] * 0.5 * szImg_Z - tof_center[2]  ) * lor_length / fabs_dZ21 ) / (sqrt(2.)*tof_sigma) );
      // integration of the Gaussian is done on the LOR portion matching the whole current voxel along X axis
      tof_weight = 0.5 * fabs(previous_edge_erf - next_edge_erf) * tof_norm_coef;
      // keeping record of the previous edge, so as to save 1 erf computation
      previous_edge_erf = next_edge_erf;

      ap_ProjectionLine->AddVoxelInTOFBin(a_direction, 0, ( index_final ) * maskIdx1, ( x1 * y1 ) * tof_weight * maskIdx1);
      ap_ProjectionLine->AddVoxelInTOFBin(a_direction, 0, ( index_final + 1 ) * maskIdx2, ( x2 * y1 ) * tof_weight * maskIdx2);
      ap_ProjectionLine->AddVoxelInTOFBin(a_direction, 0, ( index_final + nPlane_X ) * maskIdx3, ( x1 * y2 ) * tof_weight * maskIdx3);
      ap_ProjectionLine->AddVoxelInTOFBin(a_direction, 0, ( index_final + nPlane_X + 1 ) * maskIdx4, ( x2 * y2 ) * tof_weight * maskIdx4);

      // Increment
      index_z += increment_index[ 2 ];
      intersection_z += incrementZ;
      intersection_y += incrementY;
      intersection_x += incrementX;
    }
  }

  return 0;

  
}

// =====================================================================
// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
// =====================================================================

int iProjectorJoseph::ProjectWithTOFBin(int a_direction, oProjectionLine* ap_ProjectionLine)
{
  Cerr("***** iProjectorJoseph::ProjectWithTOFBin() -> Not yet implemented !" << endl);
  return 1;
/*
  #ifdef CASTOR_DEBUG
  if (!m_initialized)
  {
    Cerr("***** iProjectorJoseph::ProjectWithTOFPos() -> Called while not initialized !" << endl);
    Exit(EXIT_DEBUG);
  }
  #endif

  #ifdef CASTOR_VERBOSE
  if (m_verbose>=10)
  {
    string direction = "";
    if (a_direction==FORWARD) direction = "forward";
    else direction = "backward";
    Cout("iProjectorJoseph::Project with TOF pos -> Project line '" << ap_ProjectionLine << "' in " << direction << " direction" << endl);
  }
  #endif

  // Get event positions
  FLTNB* event1 = ap_ProjectionLine->GetPosition1();
  FLTNB* event2 = ap_ProjectionLine->GetPosition2();
  
  // LOR length
  double lor_length = ap_ProjectionLine->GetLength();

  // Get TOF info
  double tof_resolution = ap_ProjectionLine->GetTOFResolution();
  double tof_bin_size = ap_ProjectionLine->GetTOFBinSize();
  INTNB tof_nb_bins = ap_ProjectionLine->GetNbTOFBins();
  
  double tof_sigma = tof_resolution * SPEED_OF_LIGHT * 0.5 / TWO_SQRT_TWO_LN_2;
  double tof_half_span = tof_sigma * m_TOFnbSigmas;
  
  // minimum and maximum TOF bins for this LOR
  INTNB tof_bin_index_high = (INTNB)::floor(lor_length * 0.5 / (tof_bin_size * SPEED_OF_LIGHT));
  INTNB tof_bin_index_low = - tof_bin_index_high;
  tof_bin_index_low += tof_nb_bins / 2;
  tof_bin_index_high += tof_nb_bins / 2;

  // convert delta time for the lowest bin into delta length
  double tof_deltaL = (tof_bin_index_low-tof_nb_bins/2) * tof_bin_size * SPEED_OF_LIGHT * 0.5;

  // distance between the first event1 and the center of the Gaussian distribution of the lowest TOF bin along the LOR
  double lor_tof_center = lor_length * 0.5 + tof_deltaL;

  // coefficient for conversion from erf use to integral of actual TOF function
  double tof_norm_coef = lor_length / (double)(tof_bin_index_high - tof_bin_index_low + 1);

  // Distance between point event1 and event2
  double const dX21( event2[ 0 ] - event1[ 0 ] );
  double const dY21( event2[ 1 ] - event1[ 1 ] );
  double const dZ21( event2[ 2 ] - event1[ 2 ] );

  // Compute absolute values of distances, they are used twice or more
  double const fabs_dX21 = ::fabs(dX21);
  double const fabs_dY21 = ::fabs(dY21);
  double const fabs_dZ21 = ::fabs(dZ21);

  // Size of the voxels
  double const szImg_X( mp_sizeVox[ 0 ] );
  double const szImg_Y( mp_sizeVox[ 1 ] );
  double const szImg_Z( mp_sizeVox[ 2 ] );

  // Get the number of the planes
  INTNB const nPlane_X( mp_nbVox[ 0 ] );
  INTNB const nPlane_Y( mp_nbVox[ 1 ] );
  INTNB const nPlane_Z( mp_nbVox[ 2 ] );
  INTNB const nPlane_XY( nPlane_X * nPlane_Y );

  // Get the number of the padded planes
  INTNB const nPlane_X_Pad( nPlane_X + 2 );
  INTNB const nPlane_Y_Pad( nPlane_Y + 2 );
  INTNB const nPlane_XY_Pad( nPlane_X_Pad * nPlane_Y_Pad );

  // Coordinates of the first and last planes (passing through voxel edges)
  double const xPlane_0 = -mp_halfFOV[ 0 ];
  double const yPlane_0 = -mp_halfFOV[ 1 ];
  double const zPlane_0 = -mp_halfFOV[ 2 ];
  double const xPlane_N = mp_halfFOV[ 0 ];
  double const yPlane_N = mp_halfFOV[ 1 ];
  double const zPlane_N = mp_halfFOV[ 2 ];

  // Parameters for the index
  INTNB index_x( 0 );
  INTNB index_y( 0 );
  INTNB index_z( 0 );

  // Min. Max. coordinate for histo.
  double const xPlane_0_min = xPlane_0 + szImg_X / 2.0;
  double const yPlane_0_min = yPlane_0 + szImg_Y / 2.0;
  double const zPlane_0_min = zPlane_0 + szImg_Z / 2.0;

  // Min. Max security. Do not take the last slice
  double const xPlane_0_min_safety = xPlane_0 - ( szImg_X / 2.0 );
  double const yPlane_0_min_safety = yPlane_0 - ( szImg_Y / 2.0 );
  double const zPlane_0_min_safety = zPlane_0 - ( szImg_Z / 2.0 );
  double const xPlane_N_max_safety = xPlane_N + ( szImg_X / 2.0 );
  double const yPlane_N_max_safety = yPlane_N + ( szImg_Y / 2.0 );
  double const zPlane_N_max_safety = zPlane_N + ( szImg_Z / 2.0 );

  // Incrementation of the plane
  double const increment[] = {
    event1[ 0 ] < xPlane_0 ? szImg_X : -szImg_X,
    event1[ 1 ] < yPlane_0 ? szImg_Y : -szImg_Y,
    event1[ 2 ] < zPlane_0 ? szImg_Z : -szImg_Z
  };

  INTNB const increment_index[] = {
    event1[ 0 ] < xPlane_0 ? 1 : -1,
    event1[ 1 ] < yPlane_0 ? 1 : -1,
    event1[ 2 ] < zPlane_0 ? 1 : -1
  };

  // Values of interpolation
  double x1( 0.0 ); double x2( 0.0 );
  double y1( 0.0 ); double y2( 0.0 );
  double z1( 0.0 ); double z2( 0.0 );

for (INTNB tof_bin=tof_bin_index_low; tof_bin<tof_bin_index_high; tof_bin++)
{

  // coordinates of the center of the Gaussian distribution (signed distances manage the order of event1/event2 along each axis)
  double tof_center[] = {0,0,0};
  // coordinates of the lower edge of the first voxel falling inside the truncated Gaussian distribution
  double tof_edge_low[] = {0,0,0};
  // coordinate of the upper edge of the last voxel falling inside the truncated Gaussian distribution
  double tof_edge_high[] = {0,0,0};
  // voxel index along each axis of the the first voxel falling inside the truncated Gaussian distribution
  INTNB tof_index_low[] = {0,0,0};
  // voxel index along each axis of the upper edge of the last voxel falling inside the truncated Gaussian distribution
  INTNB tof_index_high[] = {0,0,0};

  // TOF X
  tof_center[0] = event1[ 0 ] +  lor_tof_center * dX21 / lor_length;
  
  tof_edge_low[0] = tof_center[0] - tof_half_span * fabs_dX21 / lor_length;
  // make the coordinate match the lowest voxel edge
  tof_index_low[0] = max( (INTNB)::floor( (tof_edge_low[0] - xPlane_0) / szImg_X ), 0);
  // if LOR outside of field of view, return empty line
  if (tof_index_low[0]>nPlane_X-1) return 0;
  tof_edge_low[0] = (double)tof_index_low[0] * szImg_X + xPlane_0;

  tof_edge_high[0] = tof_center[0] + tof_half_span * fabs_dX21 / lor_length;
  // make the coordinate match the highest voxel edge
  tof_index_high[0] = min( (INTNB)::floor( (tof_edge_high[0] - xPlane_0) / szImg_X ), nPlane_X-1);
  // if LOR outside of field of view, return empty line
  if (tof_index_high[0]<0) return 0;
  tof_edge_high[0] = (double)(tof_index_high[0]+1) * szImg_X + xPlane_0;

  // TOF Y
  tof_center[1] = event1[ 1 ] +  lor_tof_center * dY21 / lor_length;

  tof_edge_low[1] = tof_center[1] - tof_half_span * fabs_dY21 / lor_length;
  tof_index_low[1] = max( (INTNB)::floor( (tof_edge_low[1] - yPlane_0) / szImg_Y ), 0);
  if (tof_index_low[1]>nPlane_Y-1) return 0;
  tof_edge_low[1] = (double)tof_index_low[1] * szImg_Y + yPlane_0;

  tof_edge_high[1] = tof_center[1] + tof_half_span * fabs_dY21 / lor_length;
  tof_index_high[1] = min( (INTNB)::floor( (tof_edge_high[1] - yPlane_0) / szImg_Y ), nPlane_Y-1);
  if (tof_index_high[1]<0) return 0;
  tof_edge_high[1] = (double)(tof_index_high[1]+1) * szImg_Y + yPlane_0;

  // TOF Z
  tof_center[2] = event1[ 2 ] +  lor_tof_center * dZ21 / lor_length;

  tof_edge_low[2] = tof_center[2] - tof_half_span * fabs_dZ21 / lor_length;
  tof_index_low[2] = max( (INTNB)::floor( (tof_edge_low[2] - zPlane_0) / szImg_Z ), 0);
  if (tof_index_low[2]>nPlane_Z-1) return 0;
  tof_edge_low[2] = (double)tof_index_low[2] * szImg_Z + zPlane_0;

  tof_edge_high[2] = tof_center[2] + tof_half_span * fabs_dZ21 / lor_length;
  tof_index_high[2] = min( (INTNB)::floor( (tof_edge_high[2] - zPlane_0) / szImg_Z ), nPlane_Z-1);
  if (tof_index_high[2]<0) return 0;
  tof_edge_high[2] = (double)(tof_index_high[2]+1) * szImg_Z + zPlane_0;

  // Parameter for the intersection point in each axis (intersection with voxel centers)
  // X
  double intersection_x( event1[ 0 ] < xPlane_0 ?
    tof_edge_low[0] + szImg_X / 2.0 : tof_edge_high[0] - szImg_X / 2.0 );

  // Y
  double intersection_y( event1[ 1 ] < yPlane_0 ?
    tof_edge_low[1] + szImg_Y / 2.0 : tof_edge_high[1] - szImg_Y / 2.0 );

  // Z
  double intersection_z( event1[ 2 ] < zPlane_0 ?
    tof_edge_low[2] + szImg_Z / 2.0 : tof_edge_high[2] - szImg_Z / 2.0 );


  INTNB const first_index[] = {
    event1[ 0 ] < xPlane_0 ? (INTNB)tof_index_low[0] : (INTNB)tof_index_high[0],
    event1[ 1 ] < yPlane_0 ? (INTNB)tof_index_low[1] : (INTNB)tof_index_high[1],
    event1[ 2 ] < zPlane_0 ? (INTNB)tof_index_low[2] : (INTNB)tof_index_high[2],
  };

  // Find the principal direction
  // X direction
  if( fabs_dX21 >= fabs_dY21 && fabs_dX21 >= fabs_dZ21 )
  {
    // Take the increment X
    double const incrementX = increment[ 0 ];

    // Compute first intersection in Y and Z
    double const p1X_IntersectionX1 = ( intersection_x - event1[ 0 ] );
    intersection_y = ( p1X_IntersectionX1 * dY21 / dX21 ) + event1[ 1 ];
    intersection_z = ( p1X_IntersectionX1 * dZ21 / dX21 ) + event1[ 2 ];

    // Compute the increment (next intersection minus current intersection)
    double const incrementY =
      ( ( intersection_x + incrementX - event1[ 0 ] ) * dY21 / dX21 )
      + event1[ 1 ] - intersection_y;
    double const incrementZ =
      ( ( intersection_x + incrementX - event1[ 0 ] ) * dZ21 / dX21 )
      + event1[ 2 ] - intersection_z;

    // Take first index for x
    index_x = first_index[ 0 ];

    // tof : erf of previous voxel edge - Gaussian center
    double previous_edge_erf =  erf( ( fabs( intersection_x - increment_index[ 0 ] * 0.5 *szImg_X - tof_center[0])  * lor_length / fabs_dX21 ) / ( sqrt(2.)*tof_sigma ) );

    // Loop over the Y-Z planes
    for( INTNB i = 0; i < (INTNB)(tof_index_high[0] - tof_index_low[0] + 1); ++i )
    {
      // Check if intersection outside of the image space
      if( intersection_y < ( yPlane_0_min_safety + m_toleranceY )
        || intersection_y >= ( yPlane_N_max_safety - m_toleranceY )
        || intersection_z < ( zPlane_0_min_safety + m_toleranceZ )
        || intersection_z >= ( zPlane_N_max_safety - m_toleranceZ ) )
      {
        index_x += increment_index[ 0 ];
        intersection_x += incrementX;
        intersection_y += incrementY;
        intersection_z += incrementZ;
        continue;
      }

      // Find the index in the image
      index_y = ::floor( ( intersection_y - yPlane_0_min ) / szImg_Y );
      index_z = ::floor( ( intersection_z - zPlane_0_min ) / szImg_Z );

      // Compute distance between intersection point and Y and Z boundaries
      y2 = ( intersection_y - mp_boundariesY[ index_y + 1 ] ) / szImg_Y;
      y1 = 1.0 - y2;
      z2 = ( intersection_z - mp_boundariesZ[ index_z + 1 ] ) / szImg_Z;
      z1 = 1.0 - z2;

      // Index in the image space
      INTNB index_final = index_x + index_y * nPlane_X + index_z * nPlane_XY;

      // Index in the padded image space
      INTNB index_final_pad = ( index_x + 1 ) + ( index_y + 1 ) * nPlane_X_Pad + ( index_z + 1 ) * nPlane_XY_Pad;
      INTNB maskIdx1 = mp_maskPad[ index_final_pad ];
      INTNB maskIdx2 = mp_maskPad[ index_final_pad + nPlane_X_Pad ];
      INTNB maskIdx3 = mp_maskPad[ index_final_pad + nPlane_XY_Pad ];
      INTNB maskIdx4 = mp_maskPad[ index_final_pad + nPlane_X_Pad + nPlane_XY_Pad ];

      // tof : erf of next voxel edge - Gaussian center
      double next_edge_erf = erf( ( fabs( intersection_x + increment_index[ 0 ] * 0.5 * szImg_X - tof_center[0]  ) * lor_length / fabs_dX21 ) / (sqrt(2.)*tof_sigma) );
      // integration of the Gaussian is done on the LOR portion matching the whole current voxel along X axis
      double tof_weight = 0.5 * fabs(previous_edge_erf - next_edge_erf) * tof_norm_coef ;
      // keeping record of the previous edge, so as to save 1 erf computation
      previous_edge_erf = next_edge_erf;

      ap_ProjectionLine->AddVoxelInTOFBin(a_direction, tof_bin, ( index_final ) * maskIdx1, ( y1 * z1 ) * tof_weight * maskIdx1);
      ap_ProjectionLine->AddVoxelInTOFBin(a_direction, tof_bin, ( index_final + nPlane_X ) * maskIdx2, ( y2 * z1 ) * tof_weight * maskIdx2);
      ap_ProjectionLine->AddVoxelInTOFBin(a_direction, tof_bin, ( index_final + nPlane_XY ) * maskIdx3, ( y1 * z2 ) * tof_weight * maskIdx3);
      ap_ProjectionLine->AddVoxelInTOFBin(a_direction, tof_bin, ( index_final + nPlane_X + nPlane_XY ) * maskIdx4, ( y2 * z2 ) * tof_weight * maskIdx4);

      // Increment
      index_x += increment_index[ 0 ];
      intersection_x += incrementX;
      intersection_y += incrementY;
      intersection_z += incrementZ;
    }
  } // Y direction
  else if( fabs_dY21 > fabs_dX21 && fabs_dY21 >= fabs_dZ21 )
  {
    // Take the increment Y
    double const incrementY = increment[ 1 ];

    // Compute first intersection in X and Z
    double const p1Y_IntersectionY1 = ( intersection_y - event1[ 1 ] );
    intersection_x = ( p1Y_IntersectionY1 * dX21 / dY21 ) + event1[ 0 ];
    intersection_z = ( p1Y_IntersectionY1 * dZ21 / dY21 ) + event1[ 2 ];

    // Compute the increment (next intersection minus current intersection)
    double const incrementX =
      ( ( intersection_y + incrementY - event1[ 1 ] ) * dX21 / dY21 )
      + event1[ 0 ] - intersection_x;
    double const incrementZ =
      ( ( intersection_y + incrementY - event1[ 1 ] ) * dZ21 / dY21 )
      + event1[ 2 ] - intersection_z;

    // Take first index for y
    index_y = first_index[ 1 ];

    // tof : erf of previous voxel edge - Gaussian center
    double previous_edge_erf =  erf( ( fabs( intersection_y - increment_index[ 1 ] * 0.5 *szImg_Y - tof_center[1]  ) * lor_length / fabs_dY21) / ( sqrt(2.)*tof_sigma ) );

    // Loop over the X-Z planes
    for( INTNB i = 0; i < (INTNB)(tof_index_high[1] - tof_index_low[1] + 1); ++i )
    {
      // Check if intersection outside of the image space
      if( intersection_x < ( xPlane_0_min_safety + m_toleranceX )
        || intersection_x >= ( xPlane_N_max_safety - m_toleranceX )
        || intersection_z < ( zPlane_0_min_safety + m_toleranceZ )
        || intersection_z >= ( zPlane_N_max_safety - m_toleranceZ ) )
      {
        index_y += increment_index[ 1 ];
        intersection_y += incrementY;
        intersection_x += incrementX;
        intersection_z += incrementZ;
        continue;
      }

      // Find the index in the image
      index_x = ::floor( ( intersection_x - xPlane_0_min ) / szImg_X );
      index_z = ::floor( ( intersection_z - zPlane_0_min ) / szImg_Z );

      // Compute distance between intersection point and Y and Z boundaries
      x2 = ( intersection_x - mp_boundariesX[ index_x + 1 ] ) / szImg_X;
      x1 = 1.0 - x2;
      z2 = ( intersection_z - mp_boundariesZ[ index_z + 1 ] ) / szImg_Z;
      z1 = 1.0 - z2;

      // Index in the image space
      uint32_t index_final = index_x + index_y * nPlane_X + index_z * nPlane_XY;

      // Index in the padded image space
      INTNB index_final_pad = ( index_x + 1 ) + ( index_y + 1 ) * nPlane_X_Pad + ( index_z + 1 ) * nPlane_XY_Pad;
      INTNB maskIdx1 = mp_maskPad[ index_final_pad ];
      INTNB maskIdx2 = mp_maskPad[ index_final_pad + 1 ];
      INTNB maskIdx3 = mp_maskPad[ index_final_pad + nPlane_XY_Pad ];
      INTNB maskIdx4 = mp_maskPad[ index_final_pad + 1 + nPlane_XY_Pad ];

      // tof : erf of next voxel edge - Gaussian center
      double next_edge_erf = erf( ( fabs( intersection_y + increment_index[ 1 ] * 0.5 * szImg_Y - tof_center[1]  ) * lor_length / fabs_dY21 ) / (sqrt(2.)*tof_sigma) );
      // integration of the Gaussian is done on the LOR portion matching the whole current voxel along X axis
      double tof_weight = 0.5 * fabs(previous_edge_erf - next_edge_erf) * tof_norm_coef;
      // keeping record of the previous edge, so as to save 1 erf computation
      previous_edge_erf = next_edge_erf;

      ap_ProjectionLine->AddVoxelInTOFBin(a_direction, tof_bin, ( index_final ) * maskIdx1, ( x1 * z1 ) * tof_weight * maskIdx1);
      ap_ProjectionLine->AddVoxelInTOFBin(a_direction, tof_bin, ( index_final + 1 ) * maskIdx2, ( x2 * z1 ) * tof_weight * maskIdx2);
      ap_ProjectionLine->AddVoxelInTOFBin(a_direction, tof_bin, ( index_final + nPlane_XY ) * maskIdx3, ( x1 * z2 ) * tof_weight * maskIdx3);
      ap_ProjectionLine->AddVoxelInTOFBin(a_direction, tof_bin, ( index_final + 1 + nPlane_XY ) * maskIdx4, ( x2 * z2 ) * tof_weight * maskIdx4);

      // Increment
      index_y += increment_index[ 1 ];
      intersection_y += incrementY;
      intersection_x += incrementX;
      intersection_z += incrementZ;
    }
  }
  else // Z direction
  {

    // Take the increment Z
    double const incrementZ = increment[ 2 ];

    // Compute first intersection in X and Y
    double const p1Z_IntersectionZ1 = ( intersection_z - event1[ 2 ] );
    intersection_y = ( p1Z_IntersectionZ1 * dY21 / dZ21 ) + event1[ 1 ];
    intersection_x = ( p1Z_IntersectionZ1 * dX21 / dZ21 ) + event1[ 0 ];

    // Compute the increment (next intersection minus current intersection)
    double const incrementY =
      ( ( intersection_z + incrementZ - event1[ 2 ] ) * dY21 / dZ21 )
      + event1[ 1 ] - intersection_y;
    double const incrementX =
      ( ( intersection_z + incrementZ - event1[ 2 ] ) * dX21 / dZ21 )
      + event1[ 0 ] - intersection_x;

    // Take first index for z
    index_z = first_index[ 2 ];

    // tof : erf of previous voxel edge - Gaussian center
    double previous_edge_erf =  erf( ( fabs( intersection_z - increment_index[ 2 ] * 0.5 *szImg_Z - tof_center[2]  ) * lor_length / fabs_dZ21) / ( sqrt(2.)*tof_sigma ) );

    // Loop over the Y-Z planes
    for( INTNB i = 0; i < (INTNB)(tof_index_high[2] - tof_index_low[2] + 1); ++i )
    {
      // Check if intersection outside of the image space
      if( intersection_y < ( yPlane_0_min_safety + m_toleranceY )
        || intersection_y >= ( yPlane_N_max_safety - m_toleranceY )
        || intersection_x < ( xPlane_0_min_safety + m_toleranceX )
        || intersection_x >= ( xPlane_N_max_safety - m_toleranceX ) )
      {
        index_z += increment_index[ 2 ];
        intersection_z += incrementZ;
        intersection_y += incrementY;
        intersection_x += incrementX;
        continue;
      }

      // Find the index in the image
      index_y = ::floor( ( intersection_y - yPlane_0_min ) / szImg_Y );
      index_x = ::floor( ( intersection_x - xPlane_0_min ) / szImg_X );

      // Compute distance between intersection point and Y and Z boundaries
      y2 = ( intersection_y - mp_boundariesY[ index_y + 1 ] ) / szImg_Y;
      y1 = 1.0 - y2;
      x2 = ( intersection_x - mp_boundariesX[ index_x + 1 ] ) / szImg_X;
      x1 = 1.0 - x2;

      // Index in the image space
      uint32_t index_final = index_x + index_y * nPlane_X + index_z * nPlane_XY;

      // Index in the padded image space
      INTNB index_final_pad = ( index_x + 1 ) + ( index_y + 1 ) * nPlane_X_Pad + ( index_z + 1 ) * nPlane_XY_Pad;
      INTNB maskIdx1 = mp_maskPad[ index_final_pad ];
      INTNB maskIdx2 = mp_maskPad[ index_final_pad + 1 ];
      INTNB maskIdx3 = mp_maskPad[ index_final_pad + nPlane_X_Pad ];
      INTNB maskIdx4 = mp_maskPad[ index_final_pad + 1 + nPlane_X_Pad ];

      // tof : erf of next voxel edge - Gaussian center
      double next_edge_erf = erf( ( fabs( intersection_z + increment_index[ 2 ] * 0.5 * szImg_Z - tof_center[2]  ) * lor_length / fabs_dZ21 ) / (sqrt(2.)*tof_sigma) );
      // integration of the Gaussian is done on the LOR portion matching the whole current voxel along X axis
      double tof_weight = 0.5 * fabs(previous_edge_erf - next_edge_erf) * tof_norm_coef;
      // keeping record of the previous edge, so as to save 1 erf computation
      previous_edge_erf = next_edge_erf;

      ap_ProjectionLine->AddVoxelInTOFBin(a_direction, tof_bin, ( index_final ) * maskIdx1, ( x1 * y1 ) * tof_weight * maskIdx1);
      ap_ProjectionLine->AddVoxelInTOFBin(a_direction, tof_bin, ( index_final + 1 ) * maskIdx2, ( x2 * y1 ) * tof_weight * maskIdx2);
      ap_ProjectionLine->AddVoxelInTOFBin(a_direction, tof_bin, ( index_final + nPlane_X ) * maskIdx3, ( x1 * y2 ) * tof_weight * maskIdx3);
      ap_ProjectionLine->AddVoxelInTOFBin(a_direction, tof_bin, ( index_final + nPlane_X + 1 ) * maskIdx4, ( x2 * y2 ) * tof_weight * maskIdx4);

      // Increment
      index_z += increment_index[ 2 ];
      intersection_z += incrementZ;
      intersection_y += incrementY;
      intersection_x += incrementX;
    }
  }
  
  // update TOF center along the LOR for the next TOF bin
  lor_tof_center += tof_bin_size;

}

  return 0;
*/

}

// =====================================================================
// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
// =====================================================================
