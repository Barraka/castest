#!/bin/bash

##
##
##  This is the Unix script to run the SPECT histogram benchmark of the CASToR project.
##
##

bash  benchmarks/castor_benchmark_pet_histogram/run_benchmark_pet_histogram.sh
bash  benchmarks/castor_benchmark_spect_histogram/run_benchmark_spect_histogram.sh


