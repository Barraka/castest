#!/bin/bash

##
##
##  This is the Unix script to run the SPECT histogram benchmark of the CASToR project.
##
##

###########################
# Some checks
###########################

# Check that the check_benchmark program exists
if [ ! -f check_benchmark_spect_histogram_unix ]
then
  echo "***** Check benchmark program not found ! Get a fresh benchmark from the CASToR website if possible."
  exit 1
fi
# Set the CASToR reconstruction program
recon="castor-recon"
# Test the existency of the CASToR reconstruction program in the PATH
type ${recon} > /dev/null
if [ $? != 0 ]
then
  echo "***** In order to run the benchmark script, please add the CASToR binary folder into your PATH !"
  exit 1
fi

###########################
# Command-line options
###########################

# To get help about the command-line options, run the program without argument or with '-h',
# '-help' or '--help' options. Then specific help about the different parts of the project
# can be prompted using specific help options.

# General verbose level (the verbose level relative to each specific part of the CASToR project
# can be customize separately if desired using options '-vb-XXXX'; to get specific help about
# these options, run the main program with the option '-help-misc')
verbose="-vb 2"

# The data file header (here, a spect histogram data file). You can have a look at this ascii
# header to see the different fields used by the reconstruction program.
datafile="-df benchmark_spect_histogram.cdh"
# It is possible to account for attenuation correction in SPECT. For that purpose, you must provide
# the attenuation correction map with a voxel unit in cm-1. The option -atn must be used as in the
# following option. Note that the dimensions of the attenuation map can be different from the
# reconstructed image as a interpolation will be automatically performed after reading.
mumap="-atn benchmark_spect_histogram_mumap.hdr"

# The output file base name. All files saved on the disk by the program will use this base name
# with an appended suffix and an extension. As an alternative, you can use the option '-dout'
# instead of '-fout'; this will create a folder of the provided name, and all files will be
# saved inside this folder. Note that the CASToR programs create a '.log' file which logs
# everything that is prompted on the screen.
output="-fout benchmark_spect_histogram_challenger"
# This is an option to specify that we want to save the image of the last iteration only. If not
# specified, by default, all iterations will be saved. You can alse use the '-oit' option to
# specifiy the series of iterations that you want to save. Run the reconstruction program with
# the '-help-out' option to get details about all options specific to the output settings.
last_it="-oit -1"

# Number of iterations (5) and subsets (8). Using this option, you can also define sequences
# of iterations with different number of subsets. For example '-it 2:24,1:12,10:1' will perform
# 2 iterations using 24 subsets, followed by 1 iteration using 12 subsets and followed by 10
# iterations using 1 subset.
iteration="-it 5:8"

# Number of voxels of the reconstructed image (X,Y,Z)
voxels_number="-dim 128,128,55"
# Size of the voxels in mm (X,Y,Z). Alternatively, you can use the '-fov' option to specify the
# size of the reconstructed field-of-view.
voxels_size="-vox 4.8,4.8,4.8"

# Optimizer
# The reconstruction algorithm. Here, we use an iterative optimization algorithm. If you want to
# get specific help on how to use this option or other algorithms, run the program with the
# '-help-algo' option. If you want to get the full list of all implemented optimization algorithms,
# run the program with the '-help-opti' option. From that list, you will get the description of
# how to parameterize each optimization algorithm. Here, for the MLEM algorithm, by not specifying
# any parameter, the default configuration file is used to parameterize the algorithm.
optimizer="-opti MLEM"

# The projection algorithm. Here the classic Siddon projector is used. To get help about how to use
# this option, run the program with the '-help-proj' option and to get a list of all implemented
# projectors, use the '-help-projm' option.
projector="-proj classicSiddon"

# By construction, CASToR can perform resolution modeling either using an image-based PSF, or through
# the use of complicated projectors. It cannot perform so-called sinogram-based resolution modeling
# because each event is processed independently from one to another.
# Here, we set a sieve corresponding to the PSF using a stationary Gaussian of 5mm transaxial and axial
# FWHM with 3.5 sigmas in the convolution kernel. The '-conv' option is generic so that any implemented
# convolution module can be used through this option. To get specific help, run the program with the
# '-help-imgp' option and to get the list of all implemented convolution methods, run with the
# '-help-conv' option.
sieve="-conv gaussian,5.,5.,3.5::sieve"

# Parallel computation using the OpenMP library. If CASToR was not compiled using OpenMP, a warning will
# be displayed if this option is used, specifying that only one thread will be used. Here we specify '0'
# in order to let the computer choose the number of threads with respect to the available ressources.
# One can also manually specify the number of threads to be used. To get details about the computation
# settings, run the program with the '-help-comp' option.
thread="-th 0"

# We want to mask a bit of the transaxial field-of-view when saving the image. To do that, we use the
# '-fov-out' option in which we give the percentage of the cylindrical transaxial FOV that we want to
# keep. The outside of this cylinder is masked with 0 value.
out_mask_trans="-fov-out 80."
# We want to mask the extrem slices when saving the image. To do that, we use the '-slice-out' option
# in which we give the number of slices that we want to mask at both sides of the FOV.
out_mask_axial="-slice-out 2"

###########################
# Launch the reconstruction
###########################

# Launch the benchmark
echo "=================================================================================================================="
echo "  --> Reconstruction is going on. Should not be long"
echo "=================================================================================================================="
${recon} ${verbose} ${datafile} ${mumap} ${output} ${last_it} ${iteration} ${voxels_number} ${voxels_size} ${optimizer} ${projector} ${sieve} ${thread} ${out_mask_trans} ${out_mask_axial}
# Check reconstruction status
if [ $? != 0 ]
then
  echo "***** An error occured during the reconstruction of the benchmark ! Abort."
  exit 1
fi

###########################
# Check the results
###########################

# Launch the check for the reconstructed image
echo "=================================================================================================================="
echo "  --> Check benchmark results (compare reconstructed image with the reference one)"
echo "=================================================================================================================="
./check_benchmark_spect_histogram_unix benchmark_spect_histogram_challenger_it5.hdr

# Finished
echo ""
exit 0

