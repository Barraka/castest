@echo off

::
::
:: This is the Windows script to run the PET histogram benchmark of the CASToR project.
::
::

:: ----------------------------
:: Some checks
:: ----------------------------

:: Check that the check_benchmark program exists
if not exist check_benchmark_pet_histogram_win.exe (
	echo Check benchmark program not found ! Get a fresh benchmark from the CASToR website if possible.
	exit /b
)

:: Set the CASToR reconstruction program
for %%X in (castor-recon.exe) do (set recon="%%~$PATH:X")
if not defined recon (
	echo CASToR reconstruction program is not found. Please add castor-recon.exe in your PATH !
	exit /b
)

:: ----------------------------
:: Command-line options
:: ----------------------------

:: To get help about the command-line options, run the program without argument or with '-h',
:: '-help' or '--help' options. Then specific help about the different parts of the project
:: can be prompted using specific help options.

:: General verbose level (the verbose level relative to each specific part of the CASToR project
:: can be customize separately if desired using options '-vb-XXXX'; to get specific help about
:: these options, run the main program with the option '-help-misc')
set verbose=-vb 2

:: The data file header (here, a pet histogram data file). You can have a look at this ascii
:: header to see the different fields used by the reconstruction program.
set datafile=-df benchmark_pet_histogram.cdh

:: The output file base name. All files saved on the disk by the program will use this base name
:: with an appended suffix and an extension. As an alternative, you can use the option '-dout'
:: instead of '-fout'; this will create a folder of the provided name, and all files will be
:: saved inside this folder. Note that the CASToR programs create a '.log' file which logs
:: everything that is prompted on the screen.
set output=-fout benchmark_pet_histogram_challenger
:: This is an option to specify that we want to save the image of the last iteration only. If not
:: specified, by default, all iterations will be saved. You can alse use the '-oit' option to
:: specifiy the series of iterations that you want to save. Run the reconstruction program with
:: the '-help-out' option to get details about all options specific to the output settings.
set last_it=-oit -1

:: Number of iterations (2) and subsets (24). Using this option, you can also define sequences
:: of iterations with different number of subsets. For example '-it 2:24,1:12,10:1' will perform
:: 2 iterations using 24 subsets, followed by 1 iteration using 12 subsets and followed by 10
:: iterations using 1 subset.
set iteration=-it 2:24

:: Number of voxels of the reconstructed image (X,Y,Z)
set voxels_number=-dim 112,112,109
:: Size of the voxels in mm (X,Y,Z). Alternatively, you can use the '-fov' option to specify the
:: size of the reconstructed field-of-view.
set voxels_size=-vox 2.,2.,2.027

:: The reconstruction algorithm. Here, we use an iterative optimization algorithm. If you want to
:: get specific help on how to use this option or other algorithms, run the program with the
:: '-help-algo' option. If you want to get the full list of all implemented optimization algorithms,
:: run the program with the '-help-opti' option. From that list, you will get the description of
:: how to parameterize each optimization algorithm. Here, for the NEGML algorithm, the first parameter
:: is the initial image value and the second one is the parameter 'psi' specific to this algorithm.
set optimizer=-opti NEGML,1.,1.

:: The projection algorithm. Here the Joseph projector is used. To get help about how to use this
:: option, run the program with the '-help-proj' option and to get a list of all implemented
:: projectors, use the '-help-projm' option.
set projector=-proj joseph

:: By construction, CASToR can perform resolution modeling either using an image-based PSF, or through
:: the use of complicated projectors. It cannot perform so-called sinogram-based resolution modeling
:: because each event is processed independently from one to another.
:: Here, we set an image-based PSF using a stationay Gaussian of 4mm transaxial and axial FWHM with 3.5
:: sigmas in the convolution kernel. The '-conv' option is generic so that any implemented convolution
:: module can be used through this option. To get specific help, run the program with the '-help-imgp'
:: option and to get the list of all implemented convolution methods, run with the '-help-conv' option.
set psf=-conv gaussian,4.,4.,3.5::psf

:: Parallel computation using the OpenMP library. If CASToR was not compiled using OpenMP, a warning will
:: be displayed if this option is used, specifying that only one thread will be used. Here we specify '0'
:: in order to let the computer choose the number of threads with respect to the available ressources.
:: One can also manually specify the number of threads to be used. To get details about the computation
:: settings, run the program with the '-help-comp' option.
set thread=-th 0

:: We want to mask a bit of the transaxial field-of-view when saving the image. To do that, we use the
:: '-fov-out' option in which we give the percentage of the cylindrical transaxial FOV that we want to
:: keep. The outside of this cylinder is masked with 0 value.
set out_mask_trans=-fov-out 98.
:: We want to mask the extrem slices when saving the image. To do that, we use the '-slice-out' option
:: in which we give the number of slices that we want to mask at both sides of the FOV.
set out_mask_axial=-slice-out 2

:: ----------------------------
:: Launch the reconstruction
:: ----------------------------

:: Launch the benchmark
echo =================================================================================================================
echo Reconstruction is going on. Should not be long.
echo =================================================================================================================
%recon% %verbose% %datafile% %output% %last_it% %iteration% %voxels_number% %voxels_size% %optimizer% %projector% %psf% %thread% %out_mask_trans% %out_mask_axial%

:: ----------------------------
:: Check the results
:: ----------------------------

:: Launch the check for the reconstructed image
echo =================================================================================================================
echo Check benchmark results (compare reconstructed image with the reference one)"
echo =================================================================================================================
.\check_benchmark_pet_histogram_win.exe benchmark_pet_histogram_challenger_it2.hdr

@echo on
