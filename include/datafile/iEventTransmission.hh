/*
This file is part of CASToR.

    CASToR is free software: you can redistribute it and/or modify it under the
    terms of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option) any later
    version.

    CASToR is distributed in the hope that it will be useful, but WITHOUT ANY
    WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
    details.

    You should have received a copy of the GNU General Public License along with
    CASToR (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.

Copyright 2017 all CASToR contributors listed below:

    --> current contributors: Thibaut MERLIN, Simon STUTE, Didier BENOIT, Marina FILIPOVIC
    --> past contributors: Valentin VIELZEUF

This is CASToR version 1.2.
*/

/*!
  \file
  \ingroup  datafile
  \brief    Declaration of class iEventTransmission
*/


#ifndef IEVENTTRANSMISSION_HH
#define IEVENTTRANSMISSION_HH 1

#include "gVariables.hh"
#include "vEvent.hh"



/*!
  \class   iEventTransmission
  \brief   Inherit from vEvent.
  \details Nothing is implemented yet here. The constructor throw an error for the moment.
*/
class iEventTransmission : public vEvent
{
  // -------------------------------------------------------------------
  // Constructor & Destructor
  public:
    iEventTransmission();
    virtual ~iEventTransmission();

  // -------------------------------------------------------------------
  // Public member functions
  public:
    int AllocateSpecificData() {return 0;}
    void Describe() {;}

  // -------------------------------------------------------------------
  // Public Get & Set functions
  public:
    inline void SetEventValue(int a_bin, FLTNBDATA a_value) {;}
    inline FLTNB GetEventValue(int a_bin) {return -1.;}
    inline FLTNB GetAdditiveCorrections(int a_bin) {return -1.;}
    inline FLTNB GetMultiplicativeCorrections() {return -1.;}

  // -------------------------------------------------------------------
  // Private member functions
  private:

  // -------------------------------------------------------------------
  // Data members
  private:
};

#endif
