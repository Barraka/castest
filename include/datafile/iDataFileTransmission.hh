/*
This file is part of CASToR.

    CASToR is free software: you can redistribute it and/or modify it under the
    terms of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option) any later
    version.

    CASToR is distributed in the hope that it will be useful, but WITHOUT ANY
    WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
    details.

    You should have received a copy of the GNU General Public License along with
    CASToR (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.

Copyright 2017 all CASToR contributors listed below:

    --> current contributors: Thibaut MERLIN, Simon STUTE, Didier BENOIT, Marina FILIPOVIC
    --> past contributors: Valentin VIELZEUF

This is CASToR version 1.2.
*/

/*!
  \file
  \ingroup  datafile
  \brief    Declaration of class iDataFileTransmission
*/

#ifndef IDATAFILETRANSMISSION_HH
#define IDATAFILETRANSMISSION_HH 1

#include "gVariables.hh"
#include "vDataFile.hh"
#include "vScanner.hh"




/*!
  \class   iDataFileTransmission
  \brief   Inherit from vDataFile.
  \details Nothing is implemented yet here. The constructor throw an error for the moment. \n
           All pure virtual functions are simply declared and return a dummy value.
*/
class iDataFileTransmission : public vDataFile
{
  // -------------------------------------------------------------------
  // Constructor & Destructor
  public:
    iDataFileTransmission();
    virtual ~iDataFileTransmission();


  // -------------------------------------------------------------------
  // Public member functions
  public:
    int ComputeSizeEvent() {return -1;}
    int PrepareDataFile() {return -1;}
    vEvent* GetEventFromBuffer(char* ap_buffer, int a_th) {return NULL;}
    int PROJ_InitFile() {return -1;}
    int PROJ_GetScannerSpecificParameters() {return -1;}
    int PROJ_WriteEvent(vEvent* ap_Event, int a_th) {return -1;}
    int PROJ_WriteHeader() {return -1;}


  // -------------------------------------------------------------------
  // Private member functions
  private:
    int CheckSpecificParameters() {return -1;}
    int CheckFileSizeConsistency() {return -1;}
    int ReadSpecificInfoInHeader(bool a_affectQuantificationFlag) {return -1;}
    int CheckSpecificConsistencyWithAnotherDatafile(vDataFile* ap_Datafile) {return -1;}


  // -------------------------------------------------------------------
  // Data members
  private:
};

#endif
