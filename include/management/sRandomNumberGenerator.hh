/*
This file is part of CASToR.

    CASToR is free software: you can redistribute it and/or modify it under the
    terms of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option) any later
    version.

    CASToR is distributed in the hope that it will be useful, but WITHOUT ANY
    WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
    details.

    You should have received a copy of the GNU General Public License along with
    CASToR (in file GNU_GPL.TXT). If not, see <http://www.gnu.org/licenses/>.

Copyright 2017 all CASToR contributors listed below:

    --> current contributors: Thibaut MERLIN, Simon STUTE, Didier BENOIT, Marina FILIPOVIC
    --> past contributors: Valentin VIELZEUF

This is CASToR version 1.2.
*/

/*!
  \file
  \ingroup  management
  \brief    Declaration of class sRandomNumberGenerator
*/


#ifndef SRANDOMNUMBERGENERATOR_HH
#define SRANDOMNUMBERGENERATOR_HH 1

#include "gVariables.hh"
#include "sOutputManager.hh"




/*!
  \class   sRandomNumberGenerator
  \brief   Singleton class that generate a thread-safe random generator number for openMP \n
           As singleton, it can be called from any class requiring RNGs
*/
class sRandomNumberGenerator
{
  // -------------------------------------------------------------------
  // Constructor & Destructor
  public:
    /*!
      \fn      static sRandomNumberGenerator* sRandomNumberGenerator::GetInstance()
      \brief   Instanciate the singleton object and Initialize member variables if not already done, 
               return a pointer to this object otherwise
      \return  instance of the sRandomNumberGenerator singleton
    */
    static sRandomNumberGenerator* GetInstance() 
    {
      if (mp_Instance == NULL) mp_Instance = new sRandomNumberGenerator();
      return mp_Instance;
    }


  // -------------------------------------------------------------------
  // Public member functions
  public:
    typedef mt19937 Engine; /*!< Engine using C++11 Mersenne Twister pseudo-random generator of 32-bit numbers */
    typedef uniform_real_distribution<double> Distribution;  /*!< Distribution which produces random floating-point values i, uniformly distributed on the interval [a, b) */
    /*!
      \fn      int sRandomNumberGenerator::Initialize(int a_nbThreads)
      \param   a_nbThreads
      \brief   Instanciate a number of RNG according to the number of threads used in openMP
      \details It uses a std::random_device as initial seed for each thread
      \return  0 if success, positive value otherwise
    */
    int Initialize(int a_nbThreads);
    
    /*!
      \fn      int sRandomNumberGenerator::Initialize(uint64_t a_seed, int a_nbThreads)
      \param   a_seed
      \param   a_nbThreads
      \brief   Instantiate a number of RNG according to the number of threads used in openMP, and a provided seed
      \details It uses the seed provided in argument for initialization. 
      \details If multitheading is used, the seed is just incremented for the initialization of the engine of each thread.
      \return  0 if success, positive value otherwise
    */
    int Initialize(int64_t a_seed, int a_nbThreads);
    /*!
      \fn      double sRandomNumberGenerator::GenerateRdmNber()
      \brief   Generate a random number for the thread whose index is recovered from the ompenMP function
      \return  a random generated number in [0. ; 1.)
      \todo    Perhaps getting the thread index from argument rather than directly from the function.
      \todo    But this implementation allows the RNG to be used anywhere in the code
      \todo    Perhaps create a random distribution on the fly, and offer the possibility to select lower/upper bounds via argument parameters
    */
    double GenerateRdmNber();
    /*!
      \fn      double sRandomNumberGenerator::GenerateNonThreadedRdmNber()
      \brief   Generate a random number using the not thread safe random generator, for use in sequential
               parts of an otherwise multithreaded code
      \return  double random number
    */
    double GenerateNonThreadedRdmNber();
    /*!
      \fn      Engine* sRandomNumberGenerator::GetNonThreadedGenerator()
      \brief   Get the not thread safe random generator, for use in sequential
               parts of an otherwise multithreaded code
      \return  double random number
    */
    Engine* GetNonThreadedGenerator();
    /*!
      \fn      void sRandomNumberGenerator::SetVerbose(int a_verboseLevel)
      \param   a_verboseLevel
      \brief   Set verbosity level
    */
    void SetVerbose(int a_verboseLevel) {m_verbose = a_verboseLevel;};


  // -------------------------------------------------------------------
  // Private member functions
  private:
    /*!
      \fn      sRandomNumberGenerator::sRandomNumberGenerator
      \brief   Constructor of sRandomNumberGenerator. Do nothing by default as it is a singleton clasee
    */
    sRandomNumberGenerator();
    /*!
      \fn      sRandomNumberGenerator::~sRandomNumberGenerator
      \brief   Destructor of sRandomNumberGenerator. Do nothing by default
    */
    ~sRandomNumberGenerator();
    // Prevent the compiler generating methods of copy the object :
    sRandomNumberGenerator(sRandomNumberGenerator const&);     
    void operator=(sRandomNumberGenerator const&);


  // -------------------------------------------------------------------
  // Data members
  private:
    static sRandomNumberGenerator* mp_Instance;      /*!< Unique instance of the singleton class */
    int m_verbose;                 /*!< Verbosity Level */
    vector<Engine> mp_Engines;     /*!< Multithreaded of mt199937 Engines (1 by thread) */
    Engine* mp_NonThreadedEngine;  /*!< Single random generator, not thread safe, for use in sequential parts of multithreaded code, 
                                        independently of multithreaded generators */
    Distribution mp_Distribution;  /*!< Uniform distribution */
};

#endif

